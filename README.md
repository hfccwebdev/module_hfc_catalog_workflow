HFC CATALOG WORKFLOW
====================

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting
* Business Logic

INTRODUCTION
------------

The HFC Catalog Workflow module provides mechanisms for processing and copying
course and program proposals, masters, and catalog displays for Henry Ford College
curriculum. It also provides field-level security and workflow enforcement for
course and program proposal node edit forms, as documented below.

* For a full description of the module, visit the issue page:
   https://dvc.hfcc.net/webadmin/issues/issue3656

* To submit bug reports and feature suggestions, or to track changes:
   https://dvc.hfcc.net/webadmin/project/issues/search/catalog

REQUIREMENTS
------------

This module requires the following modules:

* [Hank Data](https://bitbucket.org/hfccwebdev/module_hankdata)
* [HFC Catalog Helper](https://bitbucket.org/hfccwebdev/module_hfc_catalog_helper)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

* Configure user permissions in Administration » People » Permissions:

    - Bulk update catalog courses and programs

      Allows bulk update of all catalog course and program entries, based on
      effective term denoted in the corresponding course or program master.

    - Bypass proposal workflow

      Edit fields on proposals that have already been partially approved.

    - Edit Curriculum Management Fields

      Edit fields that are limited to only the Curriculum Specialist or
      Curriculum Committee leadership.

    - Edit Registrar fields

      Edit proposal fields specific to the Office of Registration and Records.

    - Edit S&I fields

      Edit proposal fields specific to the Office of Strategy & Information.

    - Edit division-level fields

      Edit fields that are restricted to Division leadership, typically
      Associate Deans or their Administrative Assistants.

    - Push proposed changes to masters

      Allow completed proposals to be pushed to new or existing course and
      program masters.

TROUBLESHOOTING
---------------

* In most cases, when proposal fields cannot be edited, the field help text will
  show the reason the field has been disabled.
* In most cases, when masters have not bulk updated as expected, the effective
  term is missing or has been set incorrectly.

BUSINESS LOGIC
--------------

### Course Proposals

### Program Proposals
