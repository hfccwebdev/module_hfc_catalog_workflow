<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\node\NodeInterface;

/**
 * Defines an interface for cloning content.
 */
interface ContentCloneServiceInterface {

  /**
   * Clones and saves the provided node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to clone.
   * @param string $title
   *   The new title.
   *
   * @return \Drupal\node\NodeInterface
   *   The new node.
   */
  public function execute(NodeInterface $node, $title);

}
