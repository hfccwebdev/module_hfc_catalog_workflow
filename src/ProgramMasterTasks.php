<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Language\Language;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;

/**
 * Defines the Program Master Tasks service.
 */
class ProgramMasterTasks implements ProgramMasterTasksInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\hfc_catalog_helper\CatalogUtilitiesInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Account Proxy service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $helper
   *   The Catalog Utilities service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    CatalogUtilitiesInterface $helper,
    TimeInterface $time
  ) {
    $this->currentUser = $current_user;
    $this->helper = $helper;
    $this->time = $time;
  }

  /**
   * Set the base values for all new nodes.
   */
  private function baseValues($type) {
    return [
      'type' => $type,
      'changed' => $this->time->getRequestTime(),
      'revisions' => 1,
      'status' => 1,
      'language' => Language::LANGCODE_NOT_SPECIFIED,
      'uid' => $this->currentUser->id(),
      'name' => $this->currentUser->getAccountName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pushToCatalog(Node $master, Node $catalog = NULL) {

    $supplemental = $this->helper->getSupplementalProgramInfo($master->id());

    if (empty($catalog)) {
      $values = $this->baseValues('catalog_program');
      $catalog = Node::create($values);
      $catalog->field_program_master->target_id = $master->id();
      $catalog->field_program_master->entity = $master;

      // @todo Verify HLC approval from field_hlc_approval and field_cert_hlc_waiver.
      // @see Drupal\hfc_catalog_workflow\Controller\CatalogWorkflow::pushProgramMaster()
    }

    // Program Info.
    $catalog->title->setValue($master->label());
    $catalog->field_program_status->setValue($master->field_program_status->getValue());
    $catalog->field_program_name->setValue($master->field_program_name->getValue());
    $catalog->field_program_type->setValue($master->field_program_type->getValue());
    $catalog->field_program_code->setValue($master->field_program_code->getValue());
    $catalog->field_school->setValue($master->field_school->getValue());
    $catalog->field_division->setValue($master->field_division->getValue());
    $catalog->field_department->setValue($master->field_department->getValue());
    $catalog->field_acad_level->setValue($master->field_acad_level->getValue());
    $catalog->field_employer_sponsored->setValue($master->field_employer_sponsored->getValue());
    $catalog->field_program_goals->setValue($master->field_program_goals->getValue());

    // Program Description.
    $catalog->field_program_description->setValue($master->field_program_description->getValue());
    $catalog->field_program_learning_outcomes->setValue($master->field_program_learning_outcomes->getValue());
    $catalog->field_program_career_opp->setValue($master->field_program_career_opp->getValue());
    $catalog->field_program_exposure->setValue($master->field_program_exposure->getValue());
    $catalog->field_program_licensure->setValue($master->field_program_licensure->getValue());
    $catalog->field_program_accreditation->setValue($master->field_program_accreditation->getValue());
    $catalog->field_program_completion_limits->setValue($master->field_program_completion_limits->getValue());

    // Admission Requirements.
    $catalog->field_program_admission->setValue($master->field_program_admission->getValue());
    $catalog->field_program_admission_list->setValue($master->field_program_admission_list->getValue());

    // General Education.
    $catalog->field_program_aas_ab_gen_ed->setValue($master->field_program_aas_ab_gen_ed->getValue());
    $catalog->field_program_cat_1->setValue($master->field_program_cat_1->getValue());
    $catalog->field_program_cat_2->setValue($master->field_program_cat_2->getValue());
    $catalog->field_program_cat_3->setValue($master->field_program_cat_3->getValue());
    $catalog->field_program_cat_4->setValue($master->field_program_cat_4->getValue());
    $catalog->field_program_cat_5->setValue($master->field_program_cat_5->getValue());
    $catalog->field_program_cat_6->setValue($master->field_program_cat_6->getValue());
    $catalog->field_program_cat_7->setValue($master->field_program_cat_7->getValue());
    $catalog->field_program_gen_ed_cred->setValue($master->field_program_gen_ed_cred->getValue());

    // Degree Requirements.
    $catalog->field_program_comp_tech->setValue($master->field_program_comp_tech->getValue());
    $catalog->field_program_sci_math->setValue($master->field_program_sci_math->getValue());
    $catalog->field_program_sci_math_cred->setValue($master->field_program_sci_math_cred->getValue());
    $catalog->field_program_wellness->setValue($master->field_program_wellness->getValue());
    $catalog->field_program_deg_req_cred->setValue($master->field_program_deg_req_cred->getValue());

    // Required Courses.
    $catalog->field_program_rqcor_list->setValue($master->field_program_rqcor_list->getValue());
    $catalog->field_program_rqcor_note->setValue($master->field_program_rqcor_note->getValue());
    $catalog->field_program_rqsup_list->setValue($master->field_program_rqsup_list->getValue());
    $catalog->field_program_rqsup_note->setValue($master->field_program_rqsup_note->getValue());
    $catalog->field_program_elect_note->setValue($master->field_program_elect_note->getValue());
    $catalog->field_program_elect_hours->setValue($master->field_program_elect_hours->getValue());
    $catalog->field_program_additional->setValue($master->field_program_additional->getValue());
    $catalog->field_program_total_cred->setValue($master->field_program_total_cred->getValue());

    // Dates.
    $catalog->field_program_year->setValue($master->field_program_year->getValue());
    $catalog->field_effective_term->setValue($master->field_effective_term->getValue());

    // Supplemental program info.
    if ($supplemental) {
      $catalog->field_supplemental_info->target_id = $supplemental->id();
      $catalog->field_supplemental_info->entity = $supplemental;
    }

    $catalog->setNewRevision(TRUE);
    $catalog->setRevisionCreationTime($this->time->getRequestTime());
    $catalog->setRevisionUserId($this->currentUser->id());
    $catalog->setRevisionLogMessage('Syncronized with Program Master ' . $master->id() . '.');
    $catalog->save();

    return $catalog->id();
  }

  /**
   * {@inheritdoc}
   */
  public static function catalogBulkUpdate($nids, $loop_delay, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($nids);
      $context['sandbox']['fail_count'] = 0;
    }

    // Batch cycle limit.
    // @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21form.api.php/function/callback_batch_operation/8.3.x
    $limit = 10;

    $results = [];
    $slice = array_slice($nids, $context['sandbox']['progress'], $limit);
    foreach ($slice as $nid) {
      $master = Node::load($nid);
      $catalog = \Drupal::service('hfc_catalog_helper')->getCatalogProgram($master->id());

      if (empty($catalog) && $master->field_program_status->value == 'inactive') {
        // Do not create new $catalog entries for inactive program masters.
        $result = " - skipped: ($nid)";
      }
      else {
        $nid = \Drupal::service('hfc_program_master_tasks')->pushToCatalog($master, $catalog);
        if ($nid > 0) {
          $result = " - success: ($nid)";
        }
        else {
          \Drupal::messenger()->addError(t('@title - Copy Failed', ['@title' => $master->label()]));
          $result = " - failed";
          $context['sandbox']['fail_count']++;
        }
      }
      $context['sandbox']['progress']++;
      $context['sandbox']['current_id'] = $master->id();
      $context['message'] = Html::escape($master->label());
      $context['results'][] = $master->id() . ' : ' . Html::escape($master->label()) . $result;
      usleep(!empty($loop_delay) ? $loop_delay : 50000);
    }

    // Limit batch to five failures.
    if ($context['sandbox']['fail_count'] > 4) {
      $context['sandbox']['progress'] = $context['sandbox']['max'];
    }

    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function catalogBulkUpdateFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One Program Master processed.', '@count Program Masters processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }

}
