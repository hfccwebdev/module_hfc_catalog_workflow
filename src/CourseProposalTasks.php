<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\hankdata\HankDataInterface;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\hfc_hank_api\HfcHankApiInterface;

/**
 * Defines the Course Proposal Tasks service.
 */
class CourseProposalTasks implements CourseProposalTasksInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\hankdata\HankDataInterface definition.
   *
   * @var \Drupal\hankdata\HankDataInterface
   */
  protected $hankdata;

  /**
   * Drupal\hfc_hank_api\HfcHankApiInterface definition.
   *
   * @var \Drupal\hfc_hank_api\HfcHankApiInterface
   */
  protected $hankApi;

  /**
   * Drupal\hfc_catalog_helper\CatalogUtilitiesInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Account Proxy service.
   * @param \Drupal\hankdata\HankDataInterface $hankdata
   *   The HANK Data service.
   * @param \Drupal\hfc_hank_api\HfcHankApiInterface $hank_api
   *   The HANK API service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $helper
   *   The Catalog Utilities service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    HankDataInterface $hankdata,
    HfcHankApiInterface $hank_api,
    CatalogUtilitiesInterface $helper,
    TimeInterface $time
  ) {
    $this->currentUser = $current_user;
    $this->hankdata = $hankdata;
    $this->hankApi = $hank_api;
    $this->helper = $helper;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function createNew($values) {

    // $subject, $number, $title
    $this->messenger()->addMessage($this->t('Creating new course proposal %subject-%number: %title', [
      '%subject' => $values['subject'],
      '%number' => $values['number'],
      '%title' => $values['title'],
    ]));

    $node_values = $this->baseValues('course_proposal');

    $node_values['title'] = "{$values['subject']}-{$values['number']}: {$values['title']}";

    $node = Node::create($node_values);
    $node->field_acad_level->value = $values['acad_level'];
    $node->field_crs_subject->target_id = $values['subject'];
    $node->field_crs_number->value = $values['number'];
    $node->field_crs_title->value = $values['title'];
    $node->field_crs_credit_hours->value = $values['credit_hours'];

    $node->field_crs_description->value = $values['description'];
    // @todo Gross!!! Get actual default from config!
    $node->field_crs_description->format = 'markdown';

    if (!empty($values['school'])) {
      $node->field_school->value = $values['school'];
    }
    if (!empty($values['division'])) {
      $node->field_division->target_id = $values['division'];
    }
    if (!empty($values['department'])) {
      $node->field_department->target_id = $values['department'];
    }

    if ($values['credit_hours'] < 1) {
      $contact_hours = $values['credit_hours'] * 15;
    }
    elseif ($values['credit_hours'] == 1) {
      $contact_hours = $values['credit_hours'] * 15 + 1;
    }
    else {
      $contact_hours = $values['credit_hours'] * 15 + 2;
    }
    $node->field_crs_instructor_hours->value = $contact_hours;
    $node->field_crs_student_hours->value = $contact_hours;

    $node->setNewRevision(TRUE);
    $node->setRevisionCreationTime($this->time->getRequestTime());
    $node->setRevisionUserId($this->currentUser->id());
    $node->setRevisionLogMessage('This proposal represents a new course.');

    return $node;
  }

  /**
   * {@inheritdoc}
   */
  public function cloneFromMaster($nid) {

    if (!$master = Node::load($nid)) {
      $this->messenger()->addWarning($this->t('Could not retrieve requested course master.'));
      return;
    }

    $this->messenger()->addMessage($this->t('Creating new course proposal from %title (%nid)', [
      '%title' => $master->label(),
      '%nid' => $nid,
    ]));

    $values = $this->baseValues('course_proposal');

    $proposal = Node::create($values);
    $proposal->field_course_master->target_id = $nid;
    $proposal->field_course_master->entity = $master;

    // Basic Info.
    $proposal->title->setValue($master->label());
    $proposal->field_school->setValue($master->field_school->getValue());
    $proposal->field_division->setValue($master->field_division->getValue());
    $proposal->field_department->setValue($master->field_department->getValue());
    $proposal->field_acad_level->setValue($master->field_acad_level->getValue());
    $proposal->field_crs_subject->setValue($master->field_crs_subject->getValue());
    $proposal->field_crs_number->setValue($master->field_crs_number->getValue());
    $proposal->field_crs_title->setValue($master->field_crs_title->getValue());
    $proposal->field_crs_xref->setValue($master->field_crs_xref->getValue());
    $proposal->field_crs_credit_hours->setValue($master->field_crs_credit_hours->getValue());
    $proposal->field_crs_instructor_hours->setValue($master->field_crs_instructor_hours->getValue());
    $proposal->field_crs_student_hours->setValue($master->field_crs_student_hours->getValue());
    $proposal->field_crs_grading->setValue($master->field_crs_grading->getValue());
    $proposal->field_crs_prq->setValue($master->field_crs_prq->getValue());
    $proposal->field_crs_crq->setValue($master->field_crs_crq->getValue());
    $proposal->field_crs_description->setValue($master->field_crs_description->getValue());

    // Goals, Topics, Objectives.
    $proposal->field_crs_goals->setValue($master->field_crs_goals->getValue());
    $proposal->field_crs_topics->setValue($master->field_crs_topics->getValue());
    $proposal->field_crs_lrn_objectives->setValue($master->field_crs_lrn_objectives->getValue());
    $proposal->field_crs_dtl_objectives->setValue($master->field_crs_dtl_objectives->getValue());
    $proposal->field_crs_general_info->setValue($master->field_crs_general_info->getValue());

    // Assessment and Requirements.
    $proposal->field_crs_assessment->setValue($master->field_crs_assessment->getValue());
    $proposal->field_crs_requirements->setValue($master->field_crs_requirements->getValue());
    $proposal->field_crs_texts->setValue($master->field_crs_texts->getValue());
    $proposal->field_crs_fac_qual->setValue($master->field_crs_fac_qual->getValue());

    // CPCLL.
    $proposal->field_crs_cpcll_options->setValue($master->field_crs_cpcll_options->getValue());
    $proposal->field_crs_cpcll_fee->setValue($master->field_crs_cpcll_fee->getValue());
    $proposal->field_crs_cpcll_dept_exam_score->setValue($master->field_crs_cpcll_dept_exam_score->getValue());
    $proposal->field_crs_cpcll_dept_exam->setValue($master->field_crs_cpcll_dept_exam->getValue());
    $proposal->field_crs_cpcll_other_exam_score->setValue($master->field_crs_cpcll_other_exam_score->getValue());
    $proposal->field_crs_cpcll_other_exam->setValue($master->field_crs_cpcll_other_exam->getValue());
    $proposal->field_crs_cpcll_skilled_demo->setValue($master->field_crs_cpcll_skilled_demo->getValue());
    $proposal->field_crs_cpcll_portfolio->setValue($master->field_crs_cpcll_portfolio->getValue());
    $proposal->field_crs_cpcll_license->setValue($master->field_crs_cpcll_license->getValue());
    $proposal->field_crs_cpcll_license_dtl->setValue($master->field_crs_cpcll_license_dtl->getValue());
    $proposal->field_crs_cpcll_interview->setValue($master->field_crs_cpcll_interview->getValue());
    $proposal->field_crs_cpcll_other->setValue($master->field_crs_cpcll_other->getValue());

    // ACS and Reimbursement codes.
    $proposal->field_crs_acscode->setValue($master->field_crs_acscode->getValue());
    $proposal->field_crs_reimburse_code->setValue($master->field_crs_reimburse_code->getValue());

    // Outcomes.
    $proposal->field_crs_gened->setValue($master->field_crs_gened->getValue());
    $proposal->field_crs_inst_outcomes->setValue($master->field_crs_inst_outcomes->getValue());
    $proposal->field_crs_mta_categories->setValue($master->field_crs_mta_categories->getValue());
    $proposal->field_crs_wellness->setValue($master->field_crs_wellness->getValue());

    // Supplemental.
    if ($supplemental = $this->helper->getSupplementalCourseInfo($master->id())) {
      $proposal->field_office_contact->setValue($supplemental->field_office_contact->getValue());
      $proposal->field_grading_scale->setValue($supplemental->field_grading_scale->getValue());
      $proposal->field_crs_capacity->setValue($supplemental->field_crs_capacity->getValue());
      $proposal->field_crs_course_fee->setValue($supplemental->field_crs_course_fee->getValue());
      $proposal->field_crs_fee_desc->setValue($supplemental->field_crs_fee_desc->getValue());
      $proposal->field_crs_misc_dept_fee->setValue($supplemental->field_crs_misc_dept_fee->getValue());
      $proposal->field_crs_student_audit->setValue($supplemental->field_crs_student_audit->getValue());
      $proposal->field_crs_transfer->setValue($supplemental->field_crs_transfer->getValue());
      $proposal->field_crs_waitlist->setValue($supplemental->field_crs_waitlist->getValue());
      $proposal->field_instr_methods->setValue($supplemental->field_instr_methods->getValue());
      $proposal->field_transfer_colleges->setValue($supplemental->field_transfer_colleges->getValue());
      $proposal->field_transfer_notes->setValue($supplemental->field_transfer_notes->getValue());
    }

    $proposal->setNewRevision(TRUE);
    $proposal->setRevisionCreationTime($this->time->getRequestTime());
    $proposal->setRevisionUserId($this->currentUser->id());
    $proposal->setRevisionLogMessage('This proposal was cloned from Course Master ' . $master->id() . '.');

    return $proposal;
  }

  /**
   * Set the base values for all new nodes.
   */
  private function baseValues($type) {
    return [
      'type' => $type,
      'changed' => $this->time->getRequestTime(),
      'revisions' => 1,
      'status' => 1,
      'language' => Language::LANGCODE_NOT_SPECIFIED,
      'uid' => $this->currentUser->id(),
      'name' => $this->currentUser->getAccountName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pushToMaster(Node $proposal, Node $master, Node $supplemental) {

    // Basic Info.
    $master->title->setValue($proposal->label());
    $master->field_school->setValue($proposal->field_school->getValue());
    $master->field_division->setValue($proposal->field_division->getValue());
    $master->field_department->setValue($proposal->field_department->getValue());
    $master->field_acad_level->setValue($proposal->field_acad_level->getValue());
    $master->field_crs_subject->setValue($proposal->field_crs_subject->getValue());
    $master->field_crs_number->setValue($proposal->field_crs_number->getValue());
    $master->field_crs_title->setValue($proposal->field_crs_title->getValue());
    $master->field_crs_xref->setValue($proposal->field_crs_xref->getValue());
    $master->field_crs_credit_hours->setValue($proposal->field_crs_credit_hours->getValue());
    $master->field_crs_instructor_hours->setValue($proposal->field_crs_instructor_hours->getValue());
    $master->field_crs_student_hours->setValue($proposal->field_crs_student_hours->getValue());
    $master->field_crs_grading->setValue($proposal->field_crs_grading->getValue());
    $master->field_crs_prq->setValue($proposal->field_crs_prq->getValue());
    $master->field_crs_crq->setValue($proposal->field_crs_crq->getValue());
    $master->field_crs_description->setValue($proposal->field_crs_description->getValue());

    // Goals, Topics, Objectives.
    $master->field_crs_goals->setValue($proposal->field_crs_goals->getValue());
    $master->field_crs_topics->setValue($proposal->field_crs_topics->getValue());
    $master->field_crs_lrn_objectives->setValue($proposal->field_crs_lrn_objectives->getValue());
    $master->field_crs_dtl_objectives->setValue($proposal->field_crs_dtl_objectives->getValue());
    $master->field_crs_general_info->setValue($proposal->field_crs_general_info->getValue());

    // Assessment and Requirements.
    $master->field_crs_assessment->setValue($proposal->field_crs_assessment->getValue());
    $master->field_crs_requirements->setValue($proposal->field_crs_requirements->getValue());
    $master->field_crs_texts->setValue($proposal->field_crs_texts->getValue());
    $master->field_crs_fac_qual->setValue($proposal->field_crs_fac_qual->getValue());

    // CPCLL.
    $master->field_crs_cpcll_options->setValue($proposal->field_crs_cpcll_options->getValue());
    $master->field_crs_cpcll_fee->setValue($proposal->field_crs_cpcll_fee->getValue());
    $master->field_crs_cpcll_dept_exam_score->setValue($proposal->field_crs_cpcll_dept_exam_score->getValue());
    $master->field_crs_cpcll_dept_exam->setValue($proposal->field_crs_cpcll_dept_exam->getValue());
    $master->field_crs_cpcll_other_exam_score->setValue($proposal->field_crs_cpcll_other_exam_score->getValue());
    $master->field_crs_cpcll_other_exam->setValue($proposal->field_crs_cpcll_other_exam->getValue());
    $master->field_crs_cpcll_skilled_demo->setValue($proposal->field_crs_cpcll_skilled_demo->getValue());
    $master->field_crs_cpcll_portfolio->setValue($proposal->field_crs_cpcll_portfolio->getValue());
    $master->field_crs_cpcll_license->setValue($proposal->field_crs_cpcll_license->getValue());
    $master->field_crs_cpcll_license_dtl->setValue($proposal->field_crs_cpcll_license_dtl->getValue());
    $master->field_crs_cpcll_interview->setValue($proposal->field_crs_cpcll_interview->getValue());
    $master->field_crs_cpcll_other->setValue($proposal->field_crs_cpcll_other->getValue());

    // ACS and Reimbursement codes.
    $master->field_crs_acscode->setValue($proposal->field_crs_acscode->getValue());
    $master->field_crs_reimburse_code->setValue($proposal->field_crs_reimburse_code->getValue());

    // Outcomes.
    $master->field_crs_gened->setValue($proposal->field_crs_gened->getValue());
    $master->field_crs_mta_categories->setValue($proposal->field_crs_mta_categories->getValue());
    $master->field_crs_inst_outcomes->setValue($proposal->field_crs_inst_outcomes->getValue());
    $master->field_crs_wellness->setValue($proposal->field_crs_wellness->getValue());

    // Effective and Approval Dates.
    $master->field_inactive->setValue(!empty($proposal->field_deactivation_date->value));
    $master->field_effective_term->setValue($proposal->field_effective_term->getValue());
    $master->field_deactivation_date->setValue($proposal->field_deactivation_date->getValue());
    $master->field_division_approval->setValue($proposal->field_division_approval->getValue());
    $master->field_ac_approval->setValue($proposal->field_ac_approval->getValue());
    $master->field_cc_approval->setValue($proposal->field_cc_approval->getValue());

    $master->setNewRevision(TRUE);
    $master->setRevisionCreationTime($this->time->getRequestTime());
    $master->setRevisionUserId($this->currentUser->id());
    $master->setRevisionLogMessage('Syncronized with Course Proposal ' . $proposal->id() . '.');
    $master->save();

    // Supplemental Course Info.
    $supplemental->title->setValue($proposal->label());

    $supplemental->field_course_master->target_id = $master->id();
    $supplemental->field_course_master->entity = $master;
    $supplemental->field_office_contact->setValue($proposal->field_office_contact->getValue());

    $supplemental->field_grading_scale->setValue($proposal->field_grading_scale->getValue());
    $supplemental->field_crs_capacity->setValue($proposal->field_crs_capacity->getValue());
    $supplemental->field_crs_course_fee->setValue($proposal->field_crs_course_fee->getValue());
    $supplemental->field_crs_fee_desc->setValue($proposal->field_crs_fee_desc->getValue());
    $supplemental->field_crs_misc_dept_fee->setValue($proposal->field_crs_misc_dept_fee->getValue());
    $supplemental->field_crs_student_audit->setValue($proposal->field_crs_student_audit->getValue());
    $supplemental->field_crs_transfer->setValue($proposal->field_crs_transfer->getValue());
    $supplemental->field_crs_waitlist->setValue($proposal->field_crs_waitlist->getValue());
    $supplemental->field_transfer_colleges->setValue($proposal->field_transfer_colleges->getValue());
    $supplemental->field_transfer_notes->setValue($proposal->field_transfer_notes->getValue());
    $supplemental->field_instr_methods->setValue($proposal->field_instr_methods->getValue());

    $supplemental->setNewRevision(TRUE);
    $supplemental->setRevisionCreationTime($this->time->getRequestTime());
    $supplemental->setRevisionUserId($this->currentUser->id());
    $supplemental->setRevisionLogMessage('Syncronized with Course Proposal ' . $proposal->id() . '.');
    $supplemental->save();

    if (empty($master->field_supplemental_info->target_id) && $supplemental->id()) {
      $master->field_supplemental_info->target_id = $supplemental->id();
      $master->field_supplemental_info->entity = $supplemental;

      $master->setNewRevision(TRUE);
      $master->setRevisionCreationTime($this->time->getRequestTime());
      $master->setRevisionUserId($this->currentUser->id());
      $master->setRevisionLogMessage('Set field_supplemental_info to ' . $supplemental->id() . '.');
      $master->save();
    }

    return $master->id();
  }

}
