<?php

namespace Drupal\hfc_catalog_workflow\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\hankdata\HankDataInterface;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\hfc_catalog_workflow\ProgramProposalTasksInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Program Proposal create form.
 *
 * @package Drupal\hfc_catalog_workflow\Form
 *
 * For Admin theme on this page,
 * @see https://www.drupal.org/node/2158619
 */
class ProgramProposalCreateForm extends FormBase {

  /**
   * Drupal\hankdata\HankDataInterface definition.
   *
   * @var \Drupal\hankdata\HankDataInterface
   */
  protected $hankdata;

  /**
   * Drupal\hfc_catalog_helper\CatalogUtilitiesInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\hfc_catalog_workflow\ProgramProposalTasksInterface definition.
   *
   * @var \Drupal\hfc_catalog_workflow\ProgramProposalTasksInterface
   */
  protected $programProposalTasks;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the services required to construct this class.
      $container->get('hankdata'),
      $container->get('hfc_catalog_helper'),
      $container->get('entity_type.manager'),
      $container->get('hfc_program_proposal_tasks')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(
    HankDataInterface $hankdata,
    CatalogUtilitiesInterface $helper,
    EntityTypeManagerInterface $entity_type_manager,
    ProgramProposalTasksInterface $program_proposal_tasks
  ) {
    $this->hankdata = $hankdata;
    $this->helper = $helper;
    $this->entityTypeManager = $entity_type_manager;
    $this->programProposalTasks = $program_proposal_tasks;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'program_proposal_create_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['help'] = [
      '#type' => 'item',
      '#markup' => $this->t('<strong>Note:</strong> This form is for the creation of new programs only.
        To modify an existing program, use the Proposals tab on an existing Program Master.'),
    ];
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Program Title'),
      '#description' => $this->t('This is the official title for your program and will be displayed in the catalog and on the website.'),
      '#size' => 60,
      '#maxlength' => 255,
      '#weight' => 2,
      '#required' => TRUE,
    ];
    $form['program_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Degree Type'),
      '#options' => ['' => '- none -'] + $this->helper->getDegreeTypes(),
      '#weight' => 3,
      '#required' => TRUE,
    ];
    $form['acad_level'] = [
      '#type' => 'select',
      '#title' => $this->t('Academic Level'),
      '#options' => $this->helper->getAcademicLevels(),
      '#default_value' => $this->t('UG'),
      '#weight' => 3,
      '#required' => TRUE,
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Program Description'),
      '#weight' => 4,
      '#required' => TRUE,
    ];
    $form['school'] = [
      '#type' => 'select',
      '#title' => $this->t('School'),
      '#options' => ['' => '- none -'] + FieldStorageConfig::loadByName("node", "field_school")->getSetting("allowed_values"),
      '#weight' => 4,
      '#required' => TRUE,
    ];
    $form['department'] = [
      '#type' => 'select',
      '#title' => $this->t('Department'),
      '#options' => ['' => '- none -'] + $this->hankdata->getHankDepartmentOpts(["labels" => TRUE]),
      '#weight' => 6,
      '#required' => TRUE,
    ];
    $form['program_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = [
      'title' => $form_state->getValue('title'),
      'program_type' => $form_state->getValue('program_type'),
      'acad_level' => $form_state->getValue('acad_level'),
      'description' => $form_state->getValue('description'),
      'school' => $form_state->getValue('school'),
      'department' => $form_state->getValue('department'),
    ];
    $node = $this->programProposalTasks->createNew($values);

    if (is_object($node)) {
      $node->save();
      $form_state->setRedirect('entity.node.edit_form', ['node' => $node->id()]);
    }
    else {
      $this->messenger()->addError($this->t('Could not create requested Program Proposal.'));
    }

  }

}
