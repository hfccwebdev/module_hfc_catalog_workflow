<?php

namespace Drupal\hfc_catalog_workflow\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\node\NodeInterface;
use Drupal\Core\Url;
use Drupal\Core\Session\AccountInterface;
use Drupal\hfc_catalog_workflow\CourseProposalTasksInterface;
use Drupal\hfc_catalog_workflow\ProgramProposalTasksInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Proposal Clone confirmation form.
 *
 * @package Drupal\hfc_catalog_workflow\Form
 *
 * @see https://www.drupal.org/node/1945416
 */
class ProposalCloneForm extends ConfirmFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\hfc_catalog_workflow\CourseProposalTasksInterface definition.
   *
   * @var \Drupal\hfc_catalog_workflow\CourseProposalTasksInterface
   */
  protected $courseProposalTasks;

  /**
   * Drupal\hfc_catalog_workflow\ProgramProposalTasksInterface definition.
   *
   * @var \Drupal\hfc_catalog_workflow\ProgramProposalTasksInterface
   */
  protected $programProposalTasks;

  /**
   * The proposal Title.
   *
   * @var string
   */
  protected $title;

  /**
   * The source content type label.
   *
   * @var string
   */
  protected $type;

  /**
   * The cancel URL.
   *
   * @var \Drupal\Core\Url
   */
  protected $cancel_url;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('hfc_course_proposal_tasks'),
      $container->get('hfc_program_proposal_tasks')
    );
  }

  /**
   * Creates a ProposalCloneForm object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    CourseProposalTasksInterface $course_proposal_tasks,
    ProgramProposalTasksInterface $program_proposal_tasks
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->courseProposalTasks = $course_proposal_tasks;
    $this->programProposalTasks = $program_proposal_tasks;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'proposal_clone_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to create a new proposal for %title?', ['%title' => $this->title]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->cancel_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Only do this if you are sure you want to add a new proposal!');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Copy @type to Proposal', ['@type' => $this->type]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {

    $this->title = $node->label();
    $this->cancel_url = $node->toUrl();
    $this->type = $node->type->entity->label();

    // Refuse to proceed if an existing proposal is found.
    if ($this->foundActiveProposals($node)) {
      $form['proposal_found'] = [
        '#markup' => $this->t('<p><strong>An active proposal already exists for @label. Cannot continue.</strong></p>', ['@label' => $node->label()]),
      ];
      $url = Url::fromRoute('hfc_catalog_workflow.proposal_list', ['node' => $node->id()]);
      $form['proposal_link'] = [
        '#prefix' => '<p>',
        Link::fromTextAndUrl('View all proposals for this curriculum.', $url)->toRenderable(),
        '#suffix' => '</p>',
      ];
      return $form;
    }

    $form['master'] = [
      '#type' => 'value',
      '#value' => $node,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $master = $form_state->getValue('master');

    if ($master->getType() == 'course_master') {
      $node = $this->courseProposalTasks->cloneFromMaster($master->id());
    }
    elseif ($master->getType() == 'program_master') {
      $node = $this->programProposalTasks->cloneFromMaster($master->id());
    }
    else {
      $node = NULL;
    }

    if (is_object($node)) {
      $node->save();
      $form_state->setRedirect('entity.node.edit_form', ['node' => $node->id()]);
    }
    else {
      $this->messenger()->addError($this->t('Could not create requested Proposal.'));
    }
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param Node $node
   *   Run custom access checks for this node.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(AccountInterface $account, NodeInterface $node) {
    switch ($node->getType()) {
      case 'course_master':
        return AccessResult::allowedIf($account->hasPermission('create course_proposal content'));

      case 'program_master':
        return AccessResult::allowedIf($account->hasPermission('create program_proposal content'));
    }
    return AccessResult::forbidden();
  }

  /**
   * Check for any active proposals related to this master.
   *
   * @param \Drupal\node\NodeInterface $master
   *   The master node to check.
   *
   * @return bool
   *   Returns TRUE if any active proposals found.
   *
   * @todo Fix CatalogUtilities into a proper service and consolidate all of this duplicate code!!!
   */
  private function foundActiveProposals(NodeInterface $master) {

    $proposal_type = preg_replace('/_master$/', '_proposal', $master->getType());

    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', $proposal_type)
      ->condition('field_course_master', $master->id())
      ->condition('field_proposal_processed', FALSE)
      ->accessCheck(FALSE);

    return !empty($query->execute());
  }

}
