<?php

namespace Drupal\hfc_catalog_workflow\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\hfc_catalog_workflow\ContentCloneServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Course Master Push confirmation form.
 *
 * @package Drupal\hfc_catalog_workflow\Form
 *
 * @see https://www.drupal.org/node/1945416
 */
class ContentCloneConfirmationForm extends ConfirmFormBase {

  /**
   * The content cloning service.
   *
   * @var \Drupal\hfc_catalog_workflow\ContentCloneServiceInterface
   */
  protected $cloneService;

  /**
   * The existing node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * The existing node title.
   *
   * @var string
   */
  protected $title;

  /**
   * The cancel URL.
   *
   * @var \Drupal\Core\Url
   */
  protected $cancel_url;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hfc_content_clone')
    );
  }

  /**
   * Class constructor.
   *
   * @param \Drupal\hfc_catalog_workflow\ContentCloneServiceInterface $hfc_content_clone
   *   The Content cloning service.
   */
  public function __construct(ContentCloneServiceInterface $hfc_content_clone) {
    $this->cloneService = $hfc_content_clone;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_clone_confirmation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to clone %title?', ['%title' => $this->title]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->cancel_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This will create new content.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Clone Content');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {

    $this->title = $node->label();
    $this->cancel_url = $node->toUrl();

    $form['node'] = [
      '#type' => 'value',
      '#value' => $node,
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('New title'),
      '#default_value' => "Clone of " . $node->label(),
      '#size' => 120,
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $node = $form_state->getValue('node');
    $title = $form_state->getValue('title');

    $nid = $this->cloneService->execute($node, $title);
    if ($nid > 0) {
      $url = Url::fromUserInput("/node/$nid");
      $form_state->setRedirectUrl($url);
    }
  }

}
