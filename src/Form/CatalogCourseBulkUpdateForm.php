<?php

namespace Drupal\hfc_catalog_workflow\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\hankdata\HankDataInterface;
use Drupal\hankdata\Entity\HankTerm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Courses Bulk Update launch form.
 *
 * @package Drupal\hfc_catalog_workflow\Form
 */
class CatalogCourseBulkUpdateForm extends FormBase {

  /**
   * Drupal\hankdata\HankDataInterface definition.
   *
   * @var \Drupal\hankdata\HankDataInterface
   */
  protected $hankdata;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the services required to construct this class.
      $container->get('hankdata'),
      $container->get('database')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(
    HankDataInterface $hankdata,
    Connection $database
  ) {
    $this->hankdata = $hankdata;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'catalog_course_bulk_update';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['effective_term'] = [
      '#type' => 'select',
      '#title' => $this->t('Effective Term'),
      '#description' => $this->t('Course master changes effective on or before this term will be included in this update.'),
      '#options' => ['' => '- none -'] + $this->hankdata->getHankTermOpts(),
      '#default_value' => $this->hankdata->getHankCurrentTerm(),
      '#required' => TRUE,
    ];

    $form['loop_delay'] = [
      '#title' => $this->t('Loop delay'),
      '#type' => 'number',
      '#min' => '10000',
      '#step' => '10000',
      '#required' => TRUE,
      '#default_value' => !empty($form_state->getValue('loop_delay')) ? $form_state->getValue('loop_delay') : '50000',
      '#description' => $this->t("This value controls a small delay at the end of each batch cycle. Without this delay,
        this batch may put undue strain on the web server, which is also working to serve requests for other websites.<br>
        Please specify a delay in <strong>microseconds</strong>. A minimum of 50000 microseconds is recommended,
        but you may set this value higher.
      "),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update courses'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $terms_id = $form_state->getValue('effective_term');
    $loop_delay = $form_state->getValue('loop_delay');

    $effective_term = HankTerm::load($terms_id);

    $query = $this->database->select('node', 'n')->fields('n', ['nid']);
    $query->join('node_field_data', 'd', "d.nid = n.nid AND d.vid = n.vid");
    $query->join('node__field_effective_term', 'e', "e.entity_id = n.nid AND e.revision_id = n.vid");
    $query->join('hank_terms', 't', "t.terms_id = e.field_effective_term_target_id");
    $query->condition('n.type', 'course_master');
    $query->condition('d.status', NodeInterface::PUBLISHED);
    $query->condition('t.term_start_date', $effective_term->term_start_date->value, '<=');
    $query->orderBy('d.title');
    $nids = $query->execute()->fetchCol();

    $this->messenger()->addMessage($this->t('Found %c course masters.', ['%c' => count($nids)]));

    $batch = [
      'title' => $this->t('Updating Catalog Courses...'),
      'operations' => [['\Drupal\hfc_catalog_workflow\CourseMasterTasks::catalogBulkUpdate', [$nids, $loop_delay]]],
      'finished' => '\Drupal\hfc_catalog_workflow\CourseMasterTasks::catalogBulkUpdateFinishedCallback',
    ];
    batch_set($batch);
  }

}
