<?php

namespace Drupal\hfc_catalog_workflow\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\hfc_catalog_workflow\CourseProposalTasksInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Course Proposal Push confirmation form.
 *
 * @package Drupal\hfc_catalog_workflow\Form
 *
 * @see https://www.drupal.org/node/1945416
 */
class CourseProposalPushForm extends ConfirmFormBase {

  /**
   * The proposal Title.
   *
   * @var string
   */
  protected $title;

  /**
   * The cancel URL.
   *
   * @var \Drupal\Core\Url
   */
  protected $cancelUrl;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\hfc_catalog_workflow\CourseProposalTasksInterface definition.
   *
   * @var \Drupal\hfc_catalog_workflow\CourseProposalTasksInterface
   */
  protected $courseProposalTasks;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('hfc_course_proposal_tasks'),
      $container->get('datetime.time')
    );
  }

  /**
   * Initialize the object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Account Proxy service.
   * @param \Drupal\hfc_catalog_workflow\CourseProposalTasksInterface $course_proposal_tasks
   *   The Course Proposal Tasks service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    CourseProposalTasksInterface $course_proposal_tasks,
    TimeInterface $time
  ) {
    $this->currentUser = $current_user;
    $this->courseProposalTasks = $course_proposal_tasks;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'course_proposal_push_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to push proposal %title to master?', ['%title' => $this->title]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->cancelUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Only do this if you are sure!');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Push to Course Master');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $proposal = NULL, NodeInterface $master = NULL, NodeInterface $supplemental = NULL) {

    $this->title = $proposal->label();
    $this->cancelUrl = $proposal->toUrl();

    $form['proposal'] = [
      '#type' => 'value',
      '#value' => $proposal,
    ];

    $form['master'] = [
      '#type' => 'value',
      '#value' => $master,
    ];

    $form['supplemental'] = [
      '#type' => 'value',
      '#value' => $supplemental,
    ];

    $form['proposal_title'] = [
      '#prefix' => '<p><strong>',
      '#markup' => $this->t('Processing Course Proposal %n: %t', [
        '%n' => $proposal->id(),
        '%t' => $proposal->label(),
      ]),
      '#suffix' => '</strong></p>',
    ];

    if (!empty($master)) {
      $form['master_title'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t('Course Master %n: %t will be updated.', [
          '%n' => $master->id(),
          '%t' => $master->label(),
        ]),
        '#suffix' => '</p>',
      ];
    }
    else {
      $form['master_new'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t('A new course master will be created.'),
        '#suffix' => '</p>',
      ];
    }

    if (!empty($supplemental)) {
      $form['supplemental_title'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t('Supplemental Course Info %n: %t will be updated.', [
          '%n' => $supplemental->id(),
          '%t' => $supplemental->label(),
        ]),
        '#suffix' => '</p>',
      ];
    }
    else {
      $form['supplemental_new'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t('New supplemental course info will be created.'),
        '#suffix' => '</p>',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $proposal = $form_state->getValue('proposal');
    $master = $form_state->getValue('master');
    $supplemental = $form_state->getValue('supplemental');

    // On success, returns the nid of the new or existing Course Master.
    $nid = $this->courseProposalTasks->pushToMaster($proposal, $master, $supplemental);
    if ($nid > 0) {
      $proposal->field_course_master->target_id = $nid;
      $proposal->field_proposal_processed->setValue(TRUE);
      $proposal->setNewRevision(TRUE);
      $proposal->setRevisionCreationTime($this->time->getRequestTime());
      $proposal->setRevisionUserId($this->currentUser->id());
      $proposal->setRevisionLogMessage("Pushed Proposal to Course Master $nid.");
      $proposal->save();
      if (is_object($master)) {
        $form_state->setRedirectUrl($master->toUrl());
      }
      else {
        $url = Url::fromUserInput("/node/$nid");
        $form_state->setRedirectUrl($url);
      }
    }
  }

}
