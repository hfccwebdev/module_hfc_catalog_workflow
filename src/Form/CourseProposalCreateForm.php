<?php

namespace Drupal\hfc_catalog_workflow\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\hankdata\HankDataInterface;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\hfc_catalog_workflow\CourseProposalTasksInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Course Proposal create form.
 *
 * @package Drupal\hfc_catalog_workflow\Form
 *
 * For Admin theme on this page,
 * @see https://www.drupal.org/node/2158619
 */
class CourseProposalCreateForm extends FormBase {

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Drupal\hankdata\HankDataInterface definition.
   *
   * @var Drupal\hankdata\HankDataInterface
   */
  protected $hankdata;

  /**
   * Drupal\hfc_catalog_helper\CatalogUtilitiesInterface definition.
   *
   * @var Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\hfc_catalog_workflow\CourseProposalTasksInterface definition.
   *
   * @var \Drupal\hfc_catalog_workflow\CourseProposalTasksInterface
   */
  protected $courseProposalTasks;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the services required to construct this class.
      $container->get('database'),
      $container->get('hankdata'),
      $container->get('hfc_catalog_helper'),
      $container->get('entity_type.manager'),
      $container->get('hfc_course_proposal_tasks')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(
    Connection $database,
    HankDataInterface $hankdata,
    CatalogUtilitiesInterface $helper,
    EntityTypeManagerInterface $entity_type_manager,
    CourseProposalTasksInterface $course_proposal_tasks
  ) {
    $this->database = $database;
    $this->hankdata = $hankdata;
    $this->helper = $helper;
    $this->entityTypeManager = $entity_type_manager;
    $this->courseProposalTasks = $course_proposal_tasks;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'course_proposal_create_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['help'] = [
      '#type' => 'item',
      '#markup' => $this->t('<strong>Note:</strong> This form is for the creation of new courses only.
        To modify an existing course, use the Proposals tab on an existing Course Master.'),
    ];
    $form['acad_level'] = [
      '#type' => 'select',
      '#title' => $this->t('Academic Level'),
      '#options' => $this->helper->getAcademicLevels(),
      '#default_value' => $this->t('UG'),
      '#required' => TRUE,
    ];
    $form['subject'] = [
      '#type' => 'select',
      '#title' => $this->t('Course Subject'),
      '#description' => $this->t('If a new subject code is required please contact the Registrar.'),
      '#options' => ['' => '- none -'] + $this->hankdata->getHankSubjectOpts(["active" => TRUE]),
      '#required' => TRUE,
    ];
    $form['number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Course Number'),
      '#size' => 4,
      '#maxlength' => 4,
      '#weight' => 1,
      '#required' => TRUE,
    ];
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Course Title'),
      '#description' => $this->t('This is the official title for your course and will be displayed in the catalog and on the website.'),
      '#size' => 60,
      '#maxlength' => 255,
      '#weight' => 2,
      '#required' => TRUE,
    ];
    $form['credit_hours'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Credit Hours'),
      '#size' => 2,
      '#maxlength' => 2,
      '#weight' => 7,
      '#required' => TRUE,
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Course Description'),
      '#weight' => 3,
      '#required' => TRUE,
    ];
    $form['school'] = [
      '#type' => 'select',
      '#title' => $this->t('School'),
      '#options' => ['' => '- none -'] + FieldStorageConfig::loadByName("node", "field_school")->getSetting("allowed_values"),
      '#weight' => 4,
      '#required' => TRUE,
    ];
    $form['department'] = [
      '#type' => 'select',
      '#title' => $this->t('Department'),
      '#options' => ['' => '- none -'] + $this->hankdata->getHankDepartmentOpts(["labels" => TRUE]),
      '#weight' => 6,
      '#required' => TRUE,
    ];
    $form['course_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $subject = $form_state->getValue('subject');
    $number = $form_state->getValue('number');
    $title = $form_state->getValue('title');
    $credit_hours = $form_state->getValue('credit_hours');

    if (empty($subject)) {
      $form_state->setErrorByName('subject', $this->t('Please select a course subject.'));
    }
    if (empty($number)) {
      $form_state->setErrorByName('number', $this->t('Please provide a course number.'));
    }
    elseif (strlen($number) < 3) {
      $form_state->setErrorByName('number', $this->t('New course number is too short.'));
    }
    if (empty($title)) {
      $form_state->setErrorByName('title', $this->t('Please provide a course title.'));
    }
    if (!is_numeric($credit_hours)) {
      $form_state->setErrorByName('credit_hours', $this->t('Please provide credit hours.'));
    }
    if (empty($form_state->getValue('description'))) {
      $form_state->setErrorByName('description', $this->t('Please provide a course description.'));
    }

    $matches = $this->helper->findMatchingCourses($subject, $number);

    // Report duplicates if found.
    if (!empty($matches)) {
      foreach ($matches as $key => $match) {
        $form_state->setErrorByName('new_course_' . $key, $this->t('Found matching @type: %title', $match));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = [
      'acad_level' => $form_state->getValue('acad_level'),
      'subject' => $form_state->getValue('subject'),
      'number' => $form_state->getValue('number'),
      'title' => $form_state->getValue('title'),
      'description' => $form_state->getValue('description'),
      'school' => $form_state->getValue('school'),
      'department' => $form_state->getValue('department'),
      'credit_hours' => $form_state->getValue('credit_hours'),
    ];
    $node = $this->courseProposalTasks->createNew($values);

    if (is_object($node)) {
      $node->save();
      $form_state->setRedirect('entity.node.edit_form', ['node' => $node->id()]);
    }
    else {
      $this->messenger()->addError($this->t('Could not create requested Course Proposal.'));
    }
  }

}
