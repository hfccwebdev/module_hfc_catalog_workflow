<?php

namespace Drupal\hfc_catalog_workflow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hfc_catalog_workflow\ProgramProposalTasksInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Program Rename Form.
 *
 * This form allows cloning the contents of a program
 * master to a new program proposal with a new name.
 * The new proposal will not be linked to the original
 * master in any way.
 *
 * @package Drupal\hfc_catalog_workflow\Form
 */
class ProgramRenameForm extends FormBase {

  /**
   * Stores the Program Proposal Tasks service.
   *
   * @var \Drupal\hfc_catalog_workflow\ProgramProposalTasksInterface
   */
  protected $programProposalTasks;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the services required to construct this class.
      $container->get('hfc_program_proposal_tasks')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(
    ProgramProposalTasksInterface $program_proposal_tasks
  ) {
    $this->programProposalTasks = $program_proposal_tasks;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'program_rename_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {

    $form['intro'] = [
      '#markup' => $this->t(
        '<p>
          This process will copy <strong>@title</strong> to a new renamed program proposal.<br>
          The new proposal will <em>not</em> be linked to the original master.
        </p>',
        ['@title' => $node->label()]
      ),
    ];

    $form['master'] = [
      '#type' => 'value',
      '#value' => $node,
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Program Name'),
      '#description' => $this->t('This is the official title for your program and will be displayed in the catalog and on the website.'),
      '#size' => 60,
      '#maxlength' => 255,
      '#weight' => 2,
      '#default_value' => $node->field_program_name->value,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Proposal'),
      '#weight' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $master = $form_state->getValue('master');
    $proposal = $this->programProposalTasks->cloneFromMaster($master->id());

    if (is_object($proposal)) {
      unset($proposal->field_program_master);
      unset($proposal->field_program_code);
      unset($proposal->field_majors_id);
      unset($proposal->field_maj_desc);
      $proposal->field_program_name->value = $form_state->getValue('title');

      $proposal->setRevisionLogMessage(
      sprintf(
        "This proposal was cloned and renamed from %s (%s).",
        $master->label(),
        $master->id()
      )
      );

      $proposal->save();
      $form_state->setRedirect('entity.node.edit_form', ['node' => $proposal->id()]);
    }
    else {
      $this->messenger()->addError($this->t('Could not create requested proposal.'));
    }
  }

}
