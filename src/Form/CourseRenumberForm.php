<?php

namespace Drupal\hfc_catalog_workflow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hankdata\HankDataInterface;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\hfc_catalog_workflow\CourseProposalTasksInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Course Renumber Form.
 *
 * This form allows cloning the contents of a course master
 * to a new course proposal with unique prefix and course number.
 * The new proposal will have its own Required Course Connector
 * and not be linked to the original master in any way.
 *
 * @package Drupal\hfc_catalog_workflow\Form
 */
class CourseRenumberForm extends FormBase {

  /**
   * Stores the HANK Data service.
   *
   * @var Drupal\hankdata\HankDataInterface
   */
  protected $hankdata;

  /**
   * Stores the Catalog Helper service.
   *
   * @var Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Stores the Course Proposal Tasks service.
   *
   * @var \Drupal\hfc_catalog_workflow\CourseProposalTasksInterface
   */
  protected $courseProposalTasks;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the services required to construct this class.
      $container->get('hankdata'),
      $container->get('hfc_catalog_helper'),
      $container->get('hfc_course_proposal_tasks')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(
    HankDataInterface $hankdata,
    CatalogUtilitiesInterface $helper,
    CourseProposalTasksInterface $course_proposal_tasks
  ) {
    $this->hankdata = $hankdata;
    $this->helper = $helper;
    $this->courseProposalTasks = $course_proposal_tasks;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'course_renumber_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $node = NULL) {

    $form['intro'] = [
      '#markup' => $this->t(
        '<p>
          This process will copy <strong>@title</strong> to a new renumbered course proposal.<br>
          The new proposal will <em>not</em> be linked to the original master.
        </p>',
        ['@title' => $node->label()]
      ),
    ];

    $form['master'] = [
      '#type' => 'value',
      '#value' => $node,
    ];

    $form['subject'] = [
      '#type' => 'select',
      '#title' => $this->t('Course Subject'),
      '#description' => $this->t('If a new subject code is required please contact the Registrar.'),
      '#options' => ['' => '- select -'] + $this->hankdata->getHankSubjectOpts(["active" => TRUE]),
      '#default_value' => $node->field_crs_subject->target_id,
      '#required' => TRUE,
    ];
    $form['number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Course Number'),
      '#size' => 4,
      '#maxlength' => 4,
      '#weight' => 1,
      '#required' => TRUE,
    ];
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Course Title'),
      '#description' => $this->t('This is the official title for your course and will be displayed in the catalog and on the website.'),
      '#size' => 60,
      '#maxlength' => 255,
      '#weight' => 2,
      '#default_value' => $node->field_crs_title->value,
      '#required' => TRUE,
    ];
    $form['credit_hours'] = [
      '#type' => 'number',
      '#title' => $this->t('Credit Hours'),
      '#weight' => 3,
      '#default_value' => $node->field_crs_credit_hours->value,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Proposal'),
      '#weight' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $subject = $form_state->getValue('subject');
    $number = $form_state->getValue('number');
    $matches = $this->helper->findMatchingCourses($subject, $number);

    // Report duplicates if found.
    if (!empty($matches)) {
      foreach ($matches as $key => $match) {
        $form_state->setErrorByName("number_$key", $this->t('Found matching @type: %title', $match));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $master = $form_state->getValue('master');
    $proposal = $this->courseProposalTasks->cloneFromMaster($master->id());

    if (is_object($proposal)) {
      unset($proposal->field_course_master);
      $proposal->field_crs_subject->target_id = $form_state->getValue('subject');
      $proposal->field_crs_number->value = $form_state->getValue('number');
      $proposal->field_crs_title->value = $form_state->getValue('title');

      $original_credit_hours = $proposal->field_crs_credit_hours->value;
      $credit_hours = $form_state->getValue('credit_hours');

      if ((float) $credit_hours !== (float) $original_credit_hours) {

        $default_contact_hours = $this->helper->getDefaultContactHours($credit_hours);

        $proposal->field_crs_credit_hours->value = $credit_hours;
        $proposal->field_crs_instructor_hours->value = $default_contact_hours;
        $proposal->field_crs_student_hours->value = $default_contact_hours;

        $this->messenger()->addWarning($this->t(
        'Note: Setting student and instructor contact hours to @hours.',
        ['@hours' => number_format($default_contact_hours, 3)]
        ));
      }

      $proposal->setRevisionLogMessage(
      sprintf(
        "This proposal was cloned and renumbered from %s (%s).",
        $master->label(),
        $master->id()
      )
      );

      $proposal->save();
      $form_state->setRedirect('entity.node.edit_form', ['node' => $proposal->id()]);
    }
    else {
      $this->messenger()->addError($this->t('Could not create requested proposal.'));
    }
  }

}
