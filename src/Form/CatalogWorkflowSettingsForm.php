<?php

namespace Drupal\hfc_catalog_workflow\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\hfc_catalog_workflow\CatalogWorkflowServicesInterface;

/**
 * Defines the Notification Settings Form.
 */
class CatalogWorkflowSettingsForm extends ConfigFormBase implements CatalogWorkflowServicesInterface {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfc_catalog_workflow_notification_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::NOTIFICATION_SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $notifications = $this->config(static::NOTIFICATION_SETTINGS);

    $form[] = [
      '#prefix' => '<h2>',
      '#markup' => $this->t('Proposal Notification Settings'),
      '#suffix' => '</h2>',
    ];

    $form['destination'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Destination email'),
      '#default_value' => $notifications->get('destination'),
      '#maxlength' => 248,
    ];

    $form['sender'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sender email'),
      '#default_value' => $notifications->get('sender'),
      '#maxlength' => 64,
    ];

    $form['field_division_approval_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('ILT approval notification message.'),
      '#default_value' => $notifications->get('field_division_approval_message'),
      '#description' => $this->t('
        This text will be used to create the message body for notifications.<br>
        Required tags: <em>@dept, @school, @hours, @type, @label, @description, @date, @link</em>.
      '),
      '#rows' => 8,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // @todo Verify that all required tags were included in field_division_approval_message.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve and store the configuration.
    $notifications = $this->configFactory->getEditable(static::NOTIFICATION_SETTINGS);
    foreach ([
      'destination',
      'sender',
      'field_division_approval_message',
    ] as $key) {
      $notifications->set($key, $form_state->getValue($key))->save();
    }
    parent::submitForm($form, $form_state);
  }

}
