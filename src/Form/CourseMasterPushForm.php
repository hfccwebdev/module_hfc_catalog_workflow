<?php

namespace Drupal\hfc_catalog_workflow\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\hfc_catalog_workflow\CourseMasterTasksInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Course Master Push confirmation form.
 *
 * @package Drupal\hfc_catalog_workflow\Form
 *
 * @see https://www.drupal.org/node/1945416
 */
class CourseMasterPushForm extends ConfirmFormBase {

  /**
   * The proposal Title.
   *
   * @var string
   */
  protected $title;

  /**
   * The cancel URL.
   *
   * @var \Drupal\Core\Url
   */
  protected $cancel_url;

  /**
   * Drupal\hfc_catalog_workflow\CourseMasterTasksInterface definition.
   *
   * @var \Drupal\hfc_catalog_workflow\CourseMasterTasksInterface
   */
  protected $courseMasterTasks;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hfc_course_master_tasks')
    );
  }

  /**
   * Initialize the object.
   */
  public function __construct(CourseMasterTasksInterface $course_master_tasks) {
    $this->courseMasterTasks = $course_master_tasks;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'course_master_push_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to push master %title to catalog?', ['%title' => $this->title]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->cancel_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Only do this if you are sure!');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Push to Course Catalog');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, NodeInterface $master = NULL, NodeInterface $catalog = NULL) {

    $this->title = $master->label();
    $this->cancel_url = $master->toUrl();

    $form['master'] = [
      '#type' => 'value',
      '#value' => $master,
    ];

    $form['catalog'] = [
      '#type' => 'value',
      '#value' => $catalog,
    ];

    $form['proposal_title'] = [
      '#prefix' => '<p><strong>',
      '#markup' => $this->t('Processing Course Master %n: %t', ['%n' => $master->id(), '%t' => $master->label()]),
      '#suffix' => '</strong></p>',
    ];

    if (!empty($catalog)) {
      $form['catalog_title'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t('Catalog Course %n: %t will be updated.', ['%n' => $catalog->id(), '%t' => $catalog->label()]),
        '#suffix' => '</p>',
      ];
    }
    else {
      $form['catalog_new'] = [
        '#prefix' => '<p>',
        '#markup' => $this->t('A new Catalog Course will be created.'),
        '#suffix' => '</p>',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $master = $form_state->getValue('master');
    $catalog = $form_state->getValue('catalog');

    // On success, returns the nid of the new or existing Catalog Course.
    $nid = $this->courseMasterTasks->pushToCatalog($master, $catalog);
    if ($nid > 0) {
      if (is_object($catalog)) {
        $form_state->setRedirectUrl($catalog->toUrl());
      }
      else {
        $url = Url::fromUserInput("/node/$nid");
        $form_state->setRedirectUrl($url);
      }
    }
  }

}
