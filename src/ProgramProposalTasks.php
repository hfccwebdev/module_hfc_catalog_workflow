<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\hankdata\Entity\HankTerm;
use Drupal\hankdata\HankDataInterface;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\hfc_hank_api\HfcHankApiInterface;

/**
 * Defines the Program Proposal Tasks service.
 */
class ProgramProposalTasks implements ProgramProposalTasksInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\hankdata\HankDataInterface definition.
   *
   * @var \Drupal\hankdata\HankDataInterface
   */
  protected $hankdata;

  /**
   * Drupal\hfc_hank_api\HfcHankApiInterface definition.
   *
   * @var \Drupal\hfc_hank_api\HfcHankApiInterface
   */
  protected $hankApi;

  /**
   * Drupal\hfc_catalog_helper\CatalogUtilitiesInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Drupal\Core\Datetime\DateFormatterInterface definition.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Account Proxy service.
   * @param \Drupal\hankdata\HankDataInterface $hankdata
   *   The HANK Data service.
   * @param \Drupal\hfc_hank_api\HfcHankApiInterface $hank_api
   *   The HANK API service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $helper
   *   The Catalog Utilities service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The Date Formatter service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    HankDataInterface $hankdata,
    HfcHankApiInterface $hank_api,
    CatalogUtilitiesInterface $helper,
    DateFormatterInterface $date_formatter,
    TimeInterface $time
  ) {
    $this->currentUser = $current_user;
    $this->hankdata = $hankdata;
    $this->hankApi = $hank_api;
    $this->helper = $helper;
    $this->dateFormatter = $date_formatter;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function createNew($values) {

    $this->messenger()->addMessage($this->t('Creating new %type program proposal: %title', [
      '%title' => $values['title'],
      '%type' => $values['program_type'],
    ]));

    $node_values = $this->baseValues('program_proposal');

    $node_values['title'] = $values['title'];

    $node = Node::create($node_values);
    $node->field_program_type->value = $values['program_type'];
    $node->field_program_name->value = $values['title'];
    $node->field_acad_level->value = $values['acad_level'];
    $node->field_program_status->value = 'active1';
    $node->field_program_description->value = $values['description'];
    // @todo Gross!!! Get actual default from config!
    $node->field_program_description->format = 'markdown';

    if (!empty($values['school'])) {
      $node->field_school->value = $values['school'];
    }
    if (!empty($values['division'])) {
      $node->field_division->target_id = $values['division'];
    }
    if (!empty($values['department'])) {
      $node->field_department->target_id = $values['department'];
    }

    $node->setNewRevision(TRUE);
    $node->setRevisionCreationTime($this->time->getRequestTime());
    $node->setRevisionUserId($this->currentUser->id());
    $node->setRevisionLogMessage('This proposal represents a new program.');

    return $node;
  }

  /**
   * {@inheritdoc}
   */
  public function cloneFromMaster($nid) {

    if (!$master = Node::load($nid)) {
      $this->messenger()->addWarning($this->t('Could not retrieve requested program master.'));
      return;
    }

    $this->messenger()->addMessage($this->t('Creating new program proposal from %title (%nid)', [
      '%title' => $master->label(),
      '%nid' => $nid,
    ]));

    $values = $this->baseValues('program_proposal');

    $proposal = Node::create($values);
    $proposal->field_program_master->target_id = $nid;
    $proposal->field_program_master->entity = $master;

    // Program Info.
    $proposal->title->setValue($master->label());

    if (!in_array($master->field_program_status->value, ['active5', 'active6'])) {
      $proposal->field_program_status->setValue($master->field_program_status->getValue());
    }

    $proposal->field_program_name->setValue($master->field_program_name->getValue());
    $proposal->field_program_type->setValue($master->field_program_type->getValue());
    $proposal->field_program_code->setValue($master->field_program_code->getValue());
    $proposal->field_maj_desc->setValue($master->field_maj_desc->getValue());
    $proposal->field_majors_id->setValue($master->field_majors_id->getValue());
    $proposal->field_school->setValue($master->field_school->getValue());
    $proposal->field_division->setValue($master->field_division->getValue());
    $proposal->field_department->setValue($master->field_department->getValue());
    $proposal->field_acad_level->setValue($master->field_acad_level->getValue());
    $proposal->field_employer_sponsored->setValue($master->field_employer_sponsored->getValue());
    $proposal->field_program_goals->setValue($master->field_program_goals->getValue());
    $proposal->field_program_mission->setValue($master->field_program_mission->getValue());

    // Program Description.
    $proposal->field_program_description->setValue($master->field_program_description->getValue());
    $proposal->field_program_learning_outcomes->setValue($master->field_program_learning_outcomes->getValue());
    $proposal->field_program_assessment->setValue($master->field_program_assessment->getValue());
    $proposal->field_program_career_opp->setValue($master->field_program_career_opp->getValue());
    $proposal->field_program_exposure->setValue($master->field_program_exposure->getValue());
    $proposal->field_program_licensure->setValue($master->field_program_licensure->getValue());
    $proposal->field_program_accreditation->setValue($master->field_program_accreditation->getValue());
    $proposal->field_program_expected_duration->setValue($master->field_program_expected_duration->getValue());
    $proposal->field_program_completion_limits->setValue($master->field_program_completion_limits->getValue());
    $proposal->field_program_faculty_qual->setValue($master->field_program_faculty_qual->getValue());
    $proposal->field_program_finaid_eligible->setValue($master->field_program_finaid_eligible->getValue());

    // Admission Requirements.
    $proposal->field_program_admission->setValue($master->field_program_admission->getValue());
    $proposal->field_program_admission_list->setValue($master->field_program_admission_list->getValue());

    // General Education.
    $proposal->field_program_aas_ab_gen_ed->setValue($master->field_program_aas_ab_gen_ed->getValue());
    $proposal->field_program_cat_1->setValue($master->field_program_cat_1->getValue());
    $proposal->field_program_cat_2->setValue($master->field_program_cat_2->getValue());
    $proposal->field_program_cat_3->setValue($master->field_program_cat_3->getValue());
    $proposal->field_program_cat_4->setValue($master->field_program_cat_4->getValue());
    $proposal->field_program_cat_5->setValue($master->field_program_cat_5->getValue());
    $proposal->field_program_cat_6->setValue($master->field_program_cat_6->getValue());
    $proposal->field_program_cat_7->setValue($master->field_program_cat_7->getValue());
    $proposal->field_program_gen_ed_cred->setValue($master->field_program_gen_ed_cred->getValue());

    // Degree Requirements.
    $proposal->field_program_comp_tech->setValue($master->field_program_comp_tech->getValue());
    $proposal->field_program_sci_math->setValue($master->field_program_sci_math->getValue());
    $proposal->field_program_sci_math_cred->setValue($master->field_program_sci_math_cred->getValue());
    $proposal->field_program_wellness->setValue($master->field_program_wellness->getValue());
    $proposal->field_program_deg_req_cred->setValue($master->field_program_deg_req_cred->getValue());

    // Required Courses.
    $proposal->field_program_rqcor_list->setValue($master->field_program_rqcor_list->getValue());
    $proposal->field_program_rqcor_note->setValue($master->field_program_rqcor_note->getValue());
    $proposal->field_program_rqsup_list->setValue($master->field_program_rqsup_list->getValue());
    $proposal->field_program_rqsup_note->setValue($master->field_program_rqsup_note->getValue());
    $proposal->field_program_elect_note->setValue($master->field_program_elect_note->getValue());
    $proposal->field_program_elect_hours->setValue($master->field_program_elect_hours->getValue());
    $proposal->field_program_additional->setValue($master->field_program_additional->getValue());
    $proposal->field_program_total_cred->setValue($master->field_program_total_cred->getValue());

    if ($supplemental = $this->helper->getSupplementalProgramInfo($master->id())) {
      $proposal->field_faculty_contact->setValue($supplemental->field_faculty_contact->getValue());
      $proposal->field_office_contact->setValue($supplemental->field_office_contact->getValue());
      $proposal->field_program_cip->setValue($supplemental->field_program_cip->getValue());
      $proposal->field_program_costs_desc->setValue($supplemental->field_program_costs_desc->getValue());
      $proposal->field_program_div_dept_code->setValue($supplemental->field_program_div_dept_code->getValue());
      $proposal->field_program_est_tuit->setValue($supplemental->field_program_est_tuit->getValue());
      $proposal->field_program_last_admit->setValue($supplemental->field_program_last_admit->getValue());
      $proposal->field_program_last_grad->setValue($supplemental->field_program_last_grad->getValue());
      $proposal->field_program_occupational_code->setValue($supplemental->field_program_occupational_code->getValue());
    }

    $proposal->setNewRevision(TRUE);
    $proposal->setRevisionCreationTime($this->time->getRequestTime());
    $proposal->setRevisionUserId($this->currentUser->id());
    $proposal->setRevisionLogMessage('This proposal was cloned from Program Master ' . $master->id() . '.');

    return $proposal;
  }

  /**
   * Set the base values for all new nodes.
   */
  private function baseValues($type) {
    return [
      'type' => $type,
      'changed' => $this->time->getRequestTime(),
      'revisions' => 1,
      'status' => 1,
      'language' => Language::LANGCODE_NOT_SPECIFIED,
      'uid' => $this->currentUser->id(),
      'name' => $this->currentUser->getAccountName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pushToMaster(Node $proposal, Node $master, Node $supplemental) {

      // Calculate master field_program_year based
      // on proposal field_effective_term.
    if (!empty($proposal->field_effective_term->target_id) && $effective_term = HankTerm::load($proposal->field_effective_term->target_id)) {
      $master->field_program_year->value = $this->dateFormatter->format($effective_term->term_start_date->value, 'custom', 'Y');
    }

    // Program Info.
    $master->title->setValue($proposal->label());
    $master->field_program_status->setValue($proposal->field_program_status->getValue());
    $master->field_program_name->setValue($proposal->field_program_name->getValue());
    $master->field_program_type->setValue($proposal->field_program_type->getValue());
    $master->field_program_code->setValue($proposal->field_program_code->getValue());
    $master->field_maj_desc->setValue($proposal->field_maj_desc->getValue());
    $master->field_majors_id->setValue($proposal->field_majors_id->getValue());
    $master->field_school->setValue($proposal->field_school->getValue());
    $master->field_division->setValue($proposal->field_division->getValue());
    $master->field_department->setValue($proposal->field_department->getValue());
    $master->field_acad_level->setValue($proposal->field_acad_level->getValue());
    $master->field_employer_sponsored->setValue($proposal->field_employer_sponsored->getValue());

    // About.
    $master->field_program_goals->setValue($proposal->field_program_goals->getValue());
    $master->field_program_mission->setValue($proposal->field_program_mission->getValue());

    // Program Description.
    $master->field_program_description->setValue($proposal->field_program_description->getValue());
    $master->field_program_learning_outcomes->setValue($proposal->field_program_learning_outcomes->getValue());
    $master->field_program_assessment->setValue($proposal->field_program_assessment->getValue());
    $master->field_program_career_opp->setValue($proposal->field_program_career_opp->getValue());
    $master->field_program_exposure->setValue($proposal->field_program_exposure->getValue());
    $master->field_program_licensure->setValue($proposal->field_program_licensure->getValue());
    $master->field_program_accreditation->setValue($proposal->field_program_accreditation->getValue());
    $master->field_program_expected_duration->setValue($proposal->field_program_expected_duration->getValue());
    $master->field_program_completion_limits->setValue($proposal->field_program_completion_limits->getValue());
    $master->field_program_finaid_eligible->setValue($proposal->field_program_finaid_eligible->getValue());
    $master->field_program_faculty_qual->setValue($proposal->field_program_faculty_qual->getValue());

    // Admission Requirements.
    $master->field_program_admission->setValue($proposal->field_program_admission->getValue());
    $master->field_program_admission_list->setValue($proposal->field_program_admission_list->getValue());

    // General Education.
    $master->field_program_aas_ab_gen_ed->setValue($proposal->field_program_aas_ab_gen_ed->getValue());
    $master->field_program_cat_1->setValue($proposal->field_program_cat_1->getValue());
    $master->field_program_cat_2->setValue($proposal->field_program_cat_2->getValue());
    $master->field_program_cat_3->setValue($proposal->field_program_cat_3->getValue());
    $master->field_program_cat_4->setValue($proposal->field_program_cat_4->getValue());
    $master->field_program_cat_5->setValue($proposal->field_program_cat_5->getValue());
    $master->field_program_cat_6->setValue($proposal->field_program_cat_6->getValue());
    $master->field_program_cat_7->setValue($proposal->field_program_cat_7->getValue());
    $master->field_program_gen_ed_cred->setValue($proposal->field_program_gen_ed_cred->getValue());

    // Degree Requirements.
    $master->field_program_comp_tech->setValue($proposal->field_program_comp_tech->getValue());
    $master->field_program_sci_math->setValue($proposal->field_program_sci_math->getValue());
    $master->field_program_sci_math_cred->setValue($proposal->field_program_sci_math_cred->getValue());
    $master->field_program_wellness->setValue($proposal->field_program_wellness->getValue());
    $master->field_program_deg_req_cred->setValue($proposal->field_program_deg_req_cred->getValue());

    // Required Courses.
    $master->field_program_rqcor_list->setValue($proposal->field_program_rqcor_list->getValue());
    $master->field_program_rqcor_note->setValue($proposal->field_program_rqcor_note->getValue());
    $master->field_program_rqsup_list->setValue($proposal->field_program_rqsup_list->getValue());
    $master->field_program_rqsup_note->setValue($proposal->field_program_rqsup_note->getValue());
    $master->field_program_elect_note->setValue($proposal->field_program_elect_note->getValue());
    $master->field_program_elect_hours->setValue($proposal->field_program_elect_hours->getValue());
    $master->field_program_additional->setValue($proposal->field_program_additional->getValue());
    $master->field_program_total_cred->setValue($proposal->field_program_total_cred->getValue());

    // Program Dates.
    $master->field_effective_term->setValue($proposal->field_effective_term->getValue());
    $master->field_deactivation_date->setValue($proposal->field_deactivation_date->getValue());
    $master->field_division_approval->setValue($proposal->field_division_approval->getValue());
    $master->field_ac_approval->setValue($proposal->field_ac_approval->getValue());
    $master->field_cc_approval->setValue($proposal->field_cc_approval->getValue());

    if (!empty($proposal->field_hlc_approval->value)) {
      $master->field_hlc_approval->setValue($proposal->field_hlc_approval->getValue());
    }

    $master->setNewRevision(TRUE);
    $master->setRevisionCreationTime($this->time->getRequestTime());
    $master->setRevisionUserId($this->currentUser->id());
    $master->setRevisionLogMessage('Syncronized with Program Proposal ' . $proposal->id() . '.');
    $master->save();

    // Supplemental Program Info.
    $supplemental->title->setValue($proposal->label());

    // The following groups should only be populated for new programs.
    $supplemental->field_program_master->target_id = $master->id();
    $supplemental->field_program_master->entity = $master;

    // Contacts.
    $supplemental->field_office_contact->setValue($proposal->field_office_contact->getValue());
    $supplemental->field_faculty_contact->setValue($proposal->field_faculty_contact->getValue());

    // Codes.
    $supplemental->field_program_cip->setValue($proposal->field_program_cip->getValue());
    $supplemental->field_program_div_dept_code->setValue($proposal->field_program_div_dept_code->getValue());
    $supplemental->field_program_occupational_code->setValue($proposal->field_program_occupational_code->getValue());

    // New Program Information.
    $supplemental->field_program_est_tuit->setValue($proposal->field_program_est_tuit->getValue());
    $supplemental->field_program_costs_desc->setValue($proposal->field_program_costs_desc->getValue());

    // Deactivation Details.
    if (!empty($proposal->field_deactivation_date->getValue())) {
      $supplemental->field_program_last_admit->setValue($proposal->field_program_last_admit->getValue());
      $supplemental->field_program_last_grad->setValue($proposal->field_program_last_grad->getValue());
    }

    $supplemental->setNewRevision(TRUE);
    $supplemental->setRevisionCreationTime($this->time->getRequestTime());
    $supplemental->setRevisionUserId($this->currentUser->id());
    $supplemental->setRevisionLogMessage('Syncronized with Program Proposal ' . $proposal->id() . '.');
    $supplemental->save();

    if (empty($master->field_supplemental_info->target_id) && $supplemental->id()) {
      $master->field_supplemental_info->target_id = $supplemental->id();
      $master->field_supplemental_info->entity = $supplemental;

      $master->setNewRevision(TRUE);
      $master->setRevisionCreationTime($this->time->getRequestTime());
      $master->setRevisionUserId($this->currentUser->id());
      $master->setRevisionLogMessage('Set field_supplemental_info to ' . $supplemental->id() . '.');
      $master->save();
    }

    return $master->id();
  }

}
