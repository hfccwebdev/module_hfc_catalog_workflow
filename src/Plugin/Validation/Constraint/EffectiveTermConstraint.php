<?php

namespace Drupal\hfc_catalog_workflow\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the proposed Effective Term for the is valid.
 *
 * @Constraint(
 *   id = "EffectiveTerm",
 *   label = @Translation("Effective Term", context = "Validation"),
 * )
 */
class EffectiveTermConstraint extends Constraint {

  /**
   * Displays a message if the effective term is not Fall.
   *
   * @var string
   */
  public $effectiveTermFall = 'The effective term for modifications to existing programs @modal be Fall semester.';

  /**
   * Displays a message if the effective term has ended.
   *
   * @var string
   */
  public $effectiveTermEnded = 'The effective term of %term has already ended.';

  /**
   * Displays a message if the effective term has started.
   *
   * @var string
   */
  public $effectiveTermStarted = 'Classes for the effective term of %term have already started.';

  /**
   * Displays a message if registration for the effective term has started.
   *
   * @var string
   */
  public $effectiveTermRegStart = 'Warning: Registration for the effective term of %term has already started.';

  /**
   * Displays a message if registration for the effective term starts soon.
   *
   * @var string
   */
  public $effectiveTermRegWarning = 'Warning: Registration for the effective term of %term starts soon. Please check approval deadlines for this semester.';

}
