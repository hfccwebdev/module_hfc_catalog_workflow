<?php

namespace Drupal\hfc_catalog_workflow\Plugin\Validation\Constraint;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the EffectiveTerm constraint.
 */
class EffectiveTermConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('datetime.time'));
  }

  /**
   * Constructs a new EffectiveTermConstraintValidator.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(TimeInterface $time) {
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {

    $item = $items->first();
    if (!isset($item)) {
      return NULL;
    }

    $hankterm = $item->entity;
    $node = $item->getEntity();
    $request_time = $this->time->getRequestTime();

    if ($node->getType() == 'course_proposal') {
      $is_new = empty($node->field_course_master->target_id);
      $is_program = FALSE;
    }
    elseif ($node->getType() == 'program_proposal') {
      $is_new = empty($node->field_program_master->target_id);
      $is_program = TRUE;
    }

    $aalc = !empty($node->field_ac_approval->value);

    $n45_days = $request_time + 86400 * 45;
    $n60_days = $request_time + 86400 * 60;

    // Never throw violations or messages once a proposal has been approved.
    if (!empty($node->field_cc_approval->value)) {
      return NULL;
    }

    // Add a violation if the effective term has ended.
    if ($request_time > (int) $hankterm->term_end_date->value) {
      if (!$aalc) {
        $this->context->addViolation(
          $constraint->effectiveTermEnded,
          ['%term' => $hankterm->label()]
        );
      }
      else {
        $this->messenger()->addWarning($this->t(
          $constraint->effectiveTermEnded,
          ['%term' => $hankterm->label()]
        ));
      }
      return;
    }

    // Updates to existing programs must have a FALL effective term.
    if ($is_program && !$is_new && !preg_match('|FA$|', $item->target_id)) {
      $this->messenger()->addWarning($this->t(
        $constraint->effectiveTermFall,
        ['@modal' => 'should']
      ));
      return;
    }

    // Add a violation if the effective term has started.
    if ($request_time > (int) $hankterm->term_start_date->value) {
      if (!$aalc) {
        $this->context->addViolation(
          $constraint->effectiveTermStarted,
          ['%term' => $hankterm->label()]
        );
      }
      else {
        $this->messenger()->addWarning($this->t(
          $constraint->effectiveTermStarted,
          ['%term' => $hankterm->label()]
        ));
      }
      return;
    }

    // Add a warning if registration for the effective term has started.
    if ($hankterm->term_reg_start_date->value && $request_time > (int) $hankterm->term_reg_start_date->value) {
      $this->messenger()->addWarning($this->t(
        $constraint->effectiveTermRegStart,
        ['%term' => $hankterm->label()]
      ));
      return;
    }

    // Add a warning if registration for the effective term will start soon.
    if ($hankterm->term_reg_start_date->value && $n45_days > (int) $hankterm->term_reg_start_date->value) {
      $this->messenger()->addWarning($this->t(
        $constraint->effectiveTermRegWarning,
        ['%term' => $hankterm->label()]
      ));
      return;
    }

    // Add a warning if the effective term will start soon.
    if (!$hankterm->term_reg_start_date->value && $n60_days > (int) $hankterm->term_start_date->value) {
      $this->messenger()->addWarning($this->t(
        $constraint->effectiveTermRegWarning,
        ['%term' => $hankterm->label()]
      ));
      return;
    }
  }

}
