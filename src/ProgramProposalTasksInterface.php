<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\node\Entity\Node;

/**
 * Defines the Program Proposal Tasks Interface.
 */
interface ProgramProposalTasksInterface {

  /**
   * Create a new proposal.
   *
   * @param string[] $values
   *   Field values for the new entity.
   *
   * @return \Drupal\node\Entity\Node
   *   A new Program Proposal node.
   */
  public function createNew($values);

  /**
   * Clone a new proposal from Program Master.
   *
   * @param int $nid
   *   Node ID of the Program Master.
   *
   * @return \Drupal\node\Entity\Node
   *   A new Program Proposal node.
   */
  public function cloneFromMaster($nid);

  /**
   * Push a Proposal to Program Master and Supplemental info.
   *
   * @param \Drupal\node\Entity\Node $proposal
   *   The program proposal.
   * @param \Drupal\node\Entity\Node $master
   *   An existing Program Master.
   * @param \Drupal\node\Entity\Node $supplemental
   *   An existing Program Supplemental Info.
   *
   * @return int
   *   The node ID of the Program Master.
   */
  public function pushToMaster(Node $proposal, Node $master, Node $supplemental);

}
