<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\node\Entity\Node;

/**
 * Defines the Program Master Tasks Interface.
 */
interface ProgramMasterTasksInterface {

  /**
   * Push a Program Master to Catalog Program.
   *
   * @param \Drupal\node\Entity\Node $master
   *   An existing Program Master.
   * @param \Drupal\node\Entity\Node $catalog
   *   An existing Catalog Program.
   *
   * @return int
   *   The node ID of the Catalog Program.
   */
  public function pushToCatalog(Node $master, Node $catalog);

  /**
   * Bulk update Program Masters to Catalog Programs.
   *
   * @param int[] $nids
   *   An array of program master nids to process.
   * @param int $loop_delay
   *   Delay time between record processing, in microseconds.
   * @param array $context
   *   Batch API context information.
   */
  public static function catalogBulkUpdate($nids, $loop_delay, &$context);

  /**
   * Callback when Catalog Bulk Update is finished.
   */
  public static function catalogBulkUpdateFinishedCallback($success, $results, $operations);

}
