<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\node\Entity\Node;

/**
 *
 */
interface CourseMasterTasksInterface {

  /**
   * Push a Course Master to Catalog Course.
   *
   * @param \Drupal\node\Entity\Node $master
   *   An existing Course Master.
   * @param \Drupal\node\Entity\Node $catalog
   *   An Catalog Course.
   *
   * @return int
   *   The node ID of the Catalog Course.
   */
  public function pushToCatalog(Node $master, Node $catalog);

  /**
   * Bulk update Course Masters to Catalog Courses.
   *
   * @param int[] $nids
   *   An array of course master nids to process.
   * @param int $loop_delay
   *   Delay time between record processing, in microseconds.
   * @param array $context
   *   Batch API context information.
   */
  public static function catalogBulkUpdate($nids, $loop_delay, &$context);

  /**
   * Callback when Catalog Bulk Update is finished.
   */
  public static function catalogBulkUpdateFinishedCallback($success, $results, $operations);

}
