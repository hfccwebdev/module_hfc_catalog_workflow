<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\NodeInterface;

use Drupal\node\Entity\Node;

/**
 * Defines a service for cloning content.
 */
class ContentCloneService implements ContentCloneServiceInterface {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Constructs a new AssessmentReportTools object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Account Proxy Interface service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    TimeInterface $time
  ) {
    $this->currentUser = $current_user;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(NodeInterface $node, $title) {

    $target = $node->createDuplicate();
    if (!empty($title)) {
      $target->title->value = $title;
    }

    $target->created = $this->time->getRequestTime();
    $target->changed = $this->time->getRequestTime();
    $target->status = Node::PUBLISHED;
    $target->moderation_state = 'published';
    $target->uid = $this->currentUser->id();
    $target->name = $this->currentUser->getAccountName();

    // Clone paragraph fields.
    foreach (['field_pseudo_courses', 'field_sequence_semester', 'field_rti_courses'] as $field) {
      if (!empty($target->{$field})) {
        foreach ($target->{$field} as $key => $value) {
          $newpara = $value->entity->createDuplicate();
          $target->{$field}[$key] = $newpara;
        }
      }
    }

    // Clear academic year.
    if (!empty($target->field_acyr_list)) {
      unset($target->field_acyr_list);
    }

    // Save the target node.
    $target->setNewRevision(TRUE);
    $target->setRevisionCreationTime($this->time->getRequestTime());
    $target->setRevisionUserId($this->currentUser->id());
    $target->setRevisionLogMessage("Cloned from {$node->getType()} {$node->id()}.");
    $target->save();

    return $target->id();
  }

}
