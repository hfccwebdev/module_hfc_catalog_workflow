<?php

namespace Drupal\hfc_catalog_workflow\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class ContentCloneController.
 *
 * @package Drupal\hfc_catalog_workflow\Controller
 */
class ContentCloneController extends ControllerBase {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   Run custom access checks for this node.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(AccountInterface $account, EntityInterface $node) {
    $type = $node->getType();
    $allowed_types = ['program_sequence', 'pseudo_course', 'rti'];
    return AccessResult::allowedIf(in_array($type, $allowed_types) && $account->hasPermission("create $type content"));
  }

  /**
   * Display the cloning form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The node to be cloned.
   *
   * @return \Drupal\Core\Form\FormInterface
   *   Confirmation form.
   */
  public function view(EntityInterface $node) {
    return $this->formBuilder()->getForm('Drupal\hfc_catalog_workflow\Form\ContentCloneConfirmationForm', $node);
  }

}
