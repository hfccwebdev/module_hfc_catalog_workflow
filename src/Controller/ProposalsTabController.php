<?php

namespace Drupal\hfc_catalog_workflow\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller to render proposals tab for a Course or Program Master.
 *
 * @see Drupal\node\Controller\NodeViewController
 */
class ProposalsTabController extends EntityViewController {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $catalogHelper;

  /**
   * @var \Drupal\node\Entity\Node
   */
  protected $master;

  /**
   * @var bool
   */
  protected $active_found;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('current_user'),
      $container->get('date.formatter'),
      $container->get('hfc_catalog_helper')
    );
  }

  /**
   * Creates a ProposalsTabController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $catalog_helper
   *   The HFC Catalog Helper tools.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RendererInterface $renderer,
    AccountInterface $current_user,
    DateFormatter $date_formatter,
    CatalogUtilitiesInterface $catalog_helper
  ) {
    parent::__construct($entity_type_manager, $renderer);
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
    $this->catalogHelper = $catalog_helper;
    $this->active_found = FALSE;
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\node\Entity\Node $node
   *   Run custom access checks for this node.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(AccountInterface $account, Node $node) {
    switch ($node->getType()) {
      case 'course_master':
        return AccessResult::allowedIf($account->hasPermission('create course_proposal content'));

      case 'program_master':
        return AccessResult::allowedIf($account->hasPermission('create program_proposal content'));
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $node, $view_mode = 'teaser') {

    $build['node'] = parent::view($node, $view_mode);

    $this->master = $node;

    $build['proposals'] = [
      ['#markup' => $this->t('<h2>Existing Proposals</h2>')],
    ];

    if ($proposals = $this->getProposals()) {
      $build['proposals'][] = [
        '#theme' => 'table',
        '#header' => [
          $this->t('ID'),
          $node->getType() == 'course_master' ? $this->t('Course name') : $this->t('Program name'),
          $this->t('School approval date'),
          $this->t('AALC approval date'),
          $this->t('CC approval date'),
          $this->t('Status'),
          $this->t('Operations'),
        ],
        '#rows' => array_map([$this, 'getProposalRow'], $proposals),
      ];
    }
    else {
      $build['proposals'][] = ['#markup' => $this->t('<p>No matches found.</p>')];
    }

    $build['addnew'][] = ['#markup' => $this->t('<h2>Add new proposal</h2>')];
    if (!$this->active_found) {
      $clone_url = Url::fromRoute('hfc_catalog_workflow.proposal_clone', ['node' => $node->id()]);
      $build['addnew'][] = [
        '#prefix' => '<p>',
        'link' => Link::fromTextAndUrl('Generate new proposal to revise this master', $clone_url)->toRenderable(),
        '#suffix' => '</p>',
      ];
    }
    if ($node->getType() == 'course_master') {
      $renumber_url = Url::fromRoute('hfc_catalog_workflow.proposal_renumber', ['node' => $node->id()]);
      $build['addnew'][] = [
        '#prefix' => '<p>',
        'link' => Link::fromTextAndUrl('Generate proposal to renumber this course', $renumber_url)->toRenderable(),
        '#suffix' => '</p>',
      ];
    }
    if ($node->getType() == 'program_master') {
      $renumber_url = Url::fromRoute('hfc_catalog_workflow.proposal_rename', ['node' => $node->id()]);
      $build['addnew'][] = [
        '#prefix' => '<p>',
        'link' => Link::fromTextAndUrl('Generate proposal to rename this program', $renumber_url)->toRenderable(),
        '#suffix' => '</p>',
      ];
    }

    return $build;
  }

  /**
   * The _title_callback for the page that renders a single node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The current node.
   *
   * @return string
   *   The page title.
   */
  public function title(EntityInterface $node) {
    return $this->t('Proposals for @label', ['@label' => $node->label()]);
  }

  /**
   * Get related proposals.
   *
   * @return \Drupal\node\Entity\Node[]
   *   An array of related nodes.
   */
  private function getProposals() {

    $master_type = $this->master->getType();
    $proposal_type = preg_replace('/_master$/', '_proposal', $master_type);
    $master_field = "field_{$master_type}";

    $nids = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', $proposal_type)
      ->condition($master_field, $this->master->id())
      ->accessCheck(FALSE)
      ->sort('created', 'DESC')
      ->execute();

    if (!empty($nids)) {
      return Node::loadMultiple($nids);
    }
    else {
      return NULL;
    }
  }

  /**
   * Get proposal table row.
   */
  private function getProposalRow(EntityInterface $node) {

    $edit_url = Url::fromRoute('entity.node.edit_form', ['node' => $node->id()]);

    if ($node->field_proposal_processed->value == FALSE) {
      $this->active_found = TRUE;
    }

    return [
      $node->id(),
      $node->toLink(),
      !empty($node->field_division_approval->value) ? $this->dateFormatter->format($node->field_division_approval->value, 'custom', 'n/j/Y') : "-",
      !empty($node->field_ac_approval->value) ? $this->dateFormatter->format($node->field_ac_approval->value, 'custom', 'n/j/Y') : "-",
      !empty($node->field_cc_approval->value) ? $this->dateFormatter->format($node->field_cc_approval->value, 'custom', 'n/j/Y') : "-",
      $node->field_proposal_processed->value ? $this->t('Complete') : $this->t('Active'),
      $node->field_proposal_processed->value ? $this->t('n/a') : Link::fromTextAndUrl('edit', $edit_url)->toString(),
    ];
  }

}
