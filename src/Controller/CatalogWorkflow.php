<?php

namespace Drupal\hfc_catalog_workflow\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\node\Entity\Node;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CatalogWorkflow.
 *
 * @package Drupal\hfc_catalog_workflow\Controller
 */
class CatalogWorkflow extends ControllerBase {

  /**
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the services required to construct this class.
      $container->get('hfc_catalog_helper')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(CatalogUtilitiesInterface $helper) {
    $this->helper = $helper;
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\node\Entity\Node $node
   *   Run custom access checks for this node.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(AccountInterface $account, Node $node) {
    // Check permissions and combine that with any custom access checking needed. Pass forward
    // parameters from the route and/or request as needed.
    switch ($node->getType()) {
      case 'course_proposal':
        return AccessResult::allowedIf(
          $account->hasPermission('push proposals') &&
          $node->field_div_review->value &&
          !$node->field_proposal_processed->value
        );

      case 'course_master':
        return AccessResult::allowedIf($account->hasPermission('push catalog'));

      case 'program_proposal':
        return AccessResult::allowedIf(
          $account->hasPermission('push proposals') &&
          $node->field_div_review->value &&
          $node->field_pres_approval &&
          !$node->field_proposal_processed->value
        );

      case 'program_master':
        return AccessResult::allowedIf($account->hasPermission('push catalog'));
    }

    return AccessResult::forbidden();
  }

  /**
   * Push an eligible node.
   *
   * Access control check should have already verified eligibility.
   *
   * @see CatalogWorkflow::access()
   *
   * @param \Drupal\node\Entity\Node $node
   *   The source node.
   *
   * @return \Drupal\Core\Form\FormInterface
   *   Confirmation form.
   */
  public function push(Node $node) {

    switch ($node->getType()) {
      case 'course_proposal':
        return $this->pushCourseProposal($node);

      case 'course_master';
        return $this->pushCourseMaster($node);

      case 'program_proposal':
        return $this->pushProgramProposal($node);

      case 'program_master';
        return $this->pushProgramMaster($node);

      default:
        return ['#type' => 'markup', '#markup' => $this->t('Unknown content type.')];
    }
  }

  /**
   * Prepare to push a Course Proposal Node to Course Master.
   *
   * @param \Drupal\node\Entity\Node $proposal
   *   The source node.
   *
   * @return \Drupal\Core\Form\FormInterface
   *   Confirmation form.
   */
  private function pushCourseProposal(Node $proposal) {
    if ($master = $proposal->field_course_master->entity) {
      $supplemental = $this->helper->getSupplementalCourseInfo($master->id());
    }
    else {
      $master = $this->helper->makeNewContent('course_master');
      $supplemental = $this->helper->makeNewContent('supplemental_course_info');
    }
    return $this->formBuilder()->getForm('Drupal\hfc_catalog_workflow\Form\CourseProposalPushForm', $proposal, $master, $supplemental);
  }

  /**
   * Prepare to push a Course Master Node to Catalog Course.
   *
   * @param \Drupal\node\Entity\Node $master
   *   The source node.
   *
   * @return \Drupal\Core\Form\FormInterface
   *   Confirmation Form.
   */
  private function pushCourseMaster(Node $master) {
    $catalog = $this->helper->getCatalogCourse($master->id());
    return $this->formBuilder()->getForm('Drupal\hfc_catalog_workflow\Form\CourseMasterPushForm', $master, $catalog);
  }

  /**
   * Prepare to push a Program Proposal Node to Program Master.
   *
   * @param \Drupal\node\Entity\Node $proposal
   *   The source node.
   *
   * @return \Drupal\Core\Form\FormInterface
   *   Confirmation form.
   */
  private function pushProgramProposal(Node $proposal) {
    if ($master = $proposal->field_program_master->entity) {
      $supplemental = $this->helper->getSupplementalProgramInfo($master->id());
    }
    else {
      $master = $this->helper->makeNewContent('program_master');
      $supplemental = $this->helper->makeNewContent('supplemental_program_info');
    }
    return $this->formBuilder()->getForm('Drupal\hfc_catalog_workflow\Form\ProgramProposalPushForm', $proposal, $master, $supplemental);
  }

  /**
   * Prepare to push a Program Master Node to Catalog Program.
   *
   * @param \Drupal\node\Entity\Node $master
   *   The source node.
   *
   * @return \Drupal\Core\Form\FormInterface
   *   Confirmation Form.
   */
  private function pushProgramMaster(Node $master) {
    $catalog = $this->helper->getCatalogProgram($master->id());

    $program_type = $master->field_program_type->value;
    $acad_level = $master->field_acad_level->value;
    $hlc_missing = empty($master->field_hlc_approval->value);
    if (!in_array($program_type, ['AREA', 'CERT', 'EA', 'NODG']) && $acad_level == 'UG' && $hlc_missing) {
      $this->messenger()->addWarning($this->t(
        'Reminder: HLC Approval is required before publishing new @type programs. Please add the approval date.',
        ['@type' => $program_type]
      ));
    }
    elseif ($program_type == 'CERT' && $acad_level == 'UG' && $hlc_missing) {
      $this->messenger()->addWarning($this->t(
        'Reminder: HLC Approval is required before publishing new certificate programs with > %50 new courses.
         Please add the approval date if applicable.'
      ));
    }

    return $this->formBuilder()->getForm('Drupal\hfc_catalog_workflow\Form\ProgramMasterPushForm', $master, $catalog);
  }

}
