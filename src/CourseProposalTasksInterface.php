<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\node\Entity\Node;

/**
 * Defines the Course Proposal Tasks Interface.
 */
interface CourseProposalTasksInterface {

  /**
   * Create a new proposal.
   *
   * @param string[] $values
   *   Field values for the new entity.
   *
   * @return \Drupal\node\Entity\Node
   *   A new Course Proposal node.
   */
  public function createNew($values);

  /**
   * Clone a new proposal from Course Master.
   *
   * @param int $nid
   *   Node ID of the Course Master.
   *
   * @return \Drupal\node\Entity\Node
   *   A new Course Proposal node.
   */
  public function cloneFromMaster($nid);

  /**
   * Push a Proposal to Course Master and Supplemental info.
   *
   * @param \Drupal\node\Entity\Node $proposal
   *   The course proposal.
   * @param \Drupal\node\Entity\Node $master
   *   An existing Course Master.
   * @param \Drupal\node\Entity\Node $supplemental
   *   An existing Course Supplemental Info.
   *
   * @return int
   *   The node ID of the Course Master.
   */
  public function pushToMaster(Node $proposal, Node $master, Node $supplemental);

}
