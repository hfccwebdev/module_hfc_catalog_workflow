<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\hfcglobal\Event\HfcGlobalEvents;
use Drupal\hfcglobal\Event\HfcGlobalNotification;
use Drupal\node\NodeInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Defines the Catalog Workflow Notification Service.
 */
class WorkflowNotificationService implements WorkflowNotificationServiceInterface {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Stores the Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Stores the Date Formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  private $dateFormatter;

  /**
   * Stores the Event Dispatcher service.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * Stores the HFC Catalog Helper service.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Class contstructor for Workflow Notification Service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The Date Formatter service.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The Event Dispatcher service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $helper
   *   The Catalog Helper service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $date_formatter,
    EventDispatcherInterface $event_dispatcher,
    CatalogUtilitiesInterface $helper
  ) {
    $this->configFactory = $config_factory;
    $this->dateFormatter = $date_formatter;
    $this->eventDispatcher = $event_dispatcher;
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public function notify(string $notification_type, NodeInterface $node): void {

    $nid = $node->id();
    $label = $node->label();
    $link = $node->toUrl('canonical', ['absolute' => TRUE])->toString();
    $proposal_type = $node->type->entity->label();
    $author = $node->uid->entity->field_full_name->value;
    $editor = $node->revision_uid->entity->field_full_name->value;
    $options = [
      'sender' => $this->getSender(),
    ];

    // Default values.
    $title = "$proposal_type notification for $label ($nid)";
    $destination = $this->getDestination();

    switch ($notification_type) {
      case 'field_division_approval':
        $message = $this->configFactory->get(self::NOTIFICATION_SETTINGS)->get('field_division_approval_message');

        $department = $node->field_department->entity->label();
        $school = $this->helper->getSchools()[$node->field_school->value] ?? $node->field_school->value;
        $date = $this->dateFormatter->format($node->field_division_approval->value, 'custom', 'n/j/Y');

        if ($node->getType() == 'course_proposal') {
          $hours = number_format($node->field_crs_credit_hours->value, 2);
          $type = 'course';
          $description = $node->field_crs_description->value;
        }
        else {
          $hours = number_format($node->field_program_total_cred->value, 2);
          $type = $this->helper->getDegreeTypes()[$node->field_program_type->value] ?? $node->field_program_type->value;
          $description = $node->field_program_description->value;
        }

        $body = [
          [
            '#markup' => $this->t(
              $message,
              [
                '@dept' => $department,
                '@school' => $school,
                '@hours' => $hours,
                '@type' => $type,
                '@label' => $label,
                '@description' => $description,
                '@date' => $date,
                '@link' => $link,
              ]
            ),
          ],
        ];
        break;

      default:
        // Notification is not defined. Return without sending notification.
        return;

    }

    $event = HfcGlobalNotification::create($title, $destination, $body, $options);
    $this->eventDispatcher->dispatch($event, HfcGlobalEvents::HFC_GLOBAL_NOTIFICATION);
  }

  /**
   * Gets the notification destination from configuration.
   *
   * @param string|null $notification_type
   *   The notification type being dispatched.
   *
   * @return string
   *   The destination value, usually an email address.
   */
  private function getDestination(?string $notification_type = NULL): string {
    // @todo don't check notification type unless Denise says so.
    return $this->configFactory->get(self::NOTIFICATION_SETTINGS)->get('destination') ?? NULL;
  }

  /**
   * Gets the notification sender address.
   *
   * @return string
   *   The sender value, usually an email address.
   */
  private function getSender(): string {
    return $this->configFactory->get(self::NOTIFICATION_SETTINGS)->get('sender') ?? NULL;
  }

}
