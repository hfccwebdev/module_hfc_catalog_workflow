<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\node\NodeInterface;

/**
 * Defines the Catalog Workflow Notification Service Interface.
 */
interface WorkflowNotificationServiceInterface extends CatalogWorkflowServicesInterface {

  /**
   * Dispatch notification events for proposal node changes.
   *
   * @param string $notification_type
   *   The notification type to dispatch.
   * @param \Drupal\node\NodeInterface $node
   *   The affected proposal.
   */
  public function notify(string $notification_type, NodeInterface $node): void;

}
