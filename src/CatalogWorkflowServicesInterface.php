<?php

namespace Drupal\hfc_catalog_workflow;

/**
 * Provides a common interface for HFC Catalog Workflow services.
 */
interface CatalogWorkflowServicesInterface {

  /**
   * The Catalog Workflow Notification Settings indentifier.
   */
  const NOTIFICATION_SETTINGS = 'hfc_catalog_workflow.notification_settings';

}
