<?php

namespace Drupal\hfc_catalog_workflow;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Language\Language;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\hfc_hank_api\HfcHankApiInterface;

/**
 * Defines the CourseMasterTasks service.
 */
class CourseMasterTasks implements CourseMasterTasksInterface {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\hfc_hank_api\HfcHankApiInterface definition.
   *
   * @var \Drupal\hfc_hank_api\HfcHankApiInterface
   */
  protected $hankApi;

  /**
   * Drupal\hfc_catalog_helper\CatalogUtilitiesInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Stores the Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $time;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The Account Proxy service.
   * @param \Drupal\hfc_hank_api\HfcHankApiInterface $hank_api
   *   The HANK API service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $helper
   *   The Catalog Utilities service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Time service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    HfcHankApiInterface $hank_api,
    CatalogUtilitiesInterface $helper,
    TimeInterface $time
  ) {
    $this->currentUser = $current_user;
    $this->hankApi = $hank_api;
    $this->helper = $helper;
    $this->time = $time;
  }

  /**
   * Set the base values for all new nodes.
   */
  private function baseValues($type) {
    return [
      'type' => $type,
      'changed' => $this->time->getRequestTime(),
      'revisions' => 1,
      'status' => 1,
      'language' => Language::LANGCODE_NOT_SPECIFIED,
      'uid' => $this->currentUser->id(),
      'name' => $this->currentUser->getAccountName(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function pushToCatalog(Node $master, Node $catalog = NULL) {

    if (empty($catalog)) {
      $values = $this->baseValues('catalog_course');
      $catalog = Node::create($values);
      $catalog->field_course_master->target_id = $master->id();
      $catalog->field_course_master->entity = $master;
    }

    // Basic Info.
    $catalog->title->setValue($master->label());
    $catalog->field_school->setValue($master->field_school->getValue());
    $catalog->field_division->setValue($master->field_division->getValue());
    $catalog->field_department->setValue($master->field_department->getValue());
    $catalog->field_acad_level->setValue($master->field_acad_level->getValue());
    $catalog->field_crs_subject->setValue($master->field_crs_subject->getValue());
    $catalog->field_crs_number->setValue($master->field_crs_number->getValue());
    $catalog->field_crs_title->setValue($master->field_crs_title->getValue());
    $catalog->field_crs_credit_hours->setValue($master->field_crs_credit_hours->getValue());
    $catalog->field_crs_description->setValue($master->field_crs_description->getValue());
    $catalog->field_crs_prq->setValue($master->field_crs_prq->getValue());
    $catalog->field_crs_crq->setValue($master->field_crs_crq->getValue());

    if (!$master->field_inactive->value) {
      $catalog->field_crs_gened->setValue($master->field_crs_gened->getValue());
      $catalog->field_crs_inst_outcomes->setValue($master->field_crs_inst_outcomes->getValue());
      $catalog->field_crs_mta_categories->setValue($master->field_crs_mta_categories->getValue());
      $catalog->field_crs_wellness->setValue($master->field_crs_wellness->getValue());
    }
    else {
      $catalog->field_crs_gened->setValue(NULL);
      $catalog->field_crs_inst_outcomes->setValue(NULL);
      $catalog->field_crs_mta_categories->setValue(NULL);
      $catalog->field_crs_wellness->setValue(0);
    }

    if (!empty($master->field_crs_student_hours->getValue())) {
      $contact_hours = ($master->field_crs_student_hours->value - 2) / 15;
    }
    else {
      $contact_hours = NULL;
      $message = 'Contact hours not set in Course Master @title.';
      $values = ['@title' => $master->label()];
      if (!$master->field_inactive->value) {
        $this->messenger()->addWarning($this->t($message, $values));
        $this->getLogger('hfc_catalog_workflow')->warning($message, $values);
      }
    }
    $catalog->field_crs_contact_hours->setValue($contact_hours);

    // Get short title from hank_courses.
    $crs_name = mb_strtoupper(trim($master->field_crs_subject->target_id) . "-" . trim($master->field_crs_number->value));
    if ($hank_course = $this->hankApi->getData("courses", $crs_name)) {
      $hank_course = reset($hank_course);
      $catalog->field_crs_short_title->setValue($hank_course->CRS_SHORT_TITLE);
    }
    else {
      if (!$master->field_inactive->value) {
        $message = 'Could not get short title from HANK for @title';
        $values = ['@title' => $master->label()];
        $this->messenger()->addWarning($this->t($message, $values));
        $this->getLogger('hfc_catalog_workflow')->warning($message, $values);
      }
    }

    // CPCLL.
    $catalog->field_crs_cpcll_options->setValue($master->field_crs_cpcll_options->getValue());
    $catalog->field_crs_cpcll_fee->setValue($master->field_crs_cpcll_fee->getValue());
    $catalog->field_crs_cpcll_dept_exam_score->setValue($master->field_crs_cpcll_dept_exam_score->getValue());
    $catalog->field_crs_cpcll_dept_exam->setValue($master->field_crs_cpcll_dept_exam->getValue());
    $catalog->field_crs_cpcll_other_exam_score->setValue($master->field_crs_cpcll_other_exam_score->getValue());
    $catalog->field_crs_cpcll_other_exam->setValue($master->field_crs_cpcll_other_exam->getValue());
    $catalog->field_crs_cpcll_skilled_demo->setValue($master->field_crs_cpcll_skilled_demo->getValue());
    $catalog->field_crs_cpcll_portfolio->setValue($master->field_crs_cpcll_portfolio->getValue());
    $catalog->field_crs_cpcll_license->setValue($master->field_crs_cpcll_license->getValue());
    $catalog->field_crs_cpcll_license_dtl->setValue($master->field_crs_cpcll_license_dtl->getValue());
    $catalog->field_crs_cpcll_interview->setValue($master->field_crs_cpcll_interview->getValue());
    $catalog->field_crs_cpcll_other->setValue($master->field_crs_cpcll_other->getValue());

    // Effective and Approval Dates.
    $catalog->field_inactive->setValue($master->field_inactive->getValue());
    $catalog->field_effective_term->setValue($master->field_effective_term->getValue());

    // Supplemental Info.
    if ($supplemental = $this->helper->getSupplementalCourseInfo($master->id())) {
      $catalog->field_supplemental_info->target_id = $supplemental->id();
      $catalog->field_supplemental_info->entity = $supplemental;
    }
    elseif (!$master->field_inactive->value) {
      $message = 'Course Master push to Catalog: could not load Supplemental Course Info for @title';
      $values = ['@title' => $master->label()];
      $this->messenger()->addError($this->t($message, $values));
      $this->getLogger('hfc_catalog_workflow')->error($message, $values);
    }

    $catalog->setNewRevision(TRUE);
    $catalog->setRevisionCreationTime($this->time->getRequestTime());
    $catalog->setRevisionUserId($this->currentUser->id());
    $catalog->setRevisionLogMessage('Syncronized with Course Master ' . $master->id() . '.');
    $catalog->save();

    return $catalog->id();
  }

  /**
   * {@inheritdoc}
   */
  public static function catalogBulkUpdate($nids, $loop_delay, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($nids);
      $context['sandbox']['fail_count'] = 0;
    }

    // Batch cycle limit.
    // @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21form.api.php/function/callback_batch_operation/8.3.x
    $limit = 10;

    $results = [];
    $slice = array_slice($nids, $context['sandbox']['progress'], $limit);
    foreach ($slice as $nid) {
      $master = Node::load($nid);
      $catalog = \Drupal::service('hfc_catalog_helper')->getCatalogCourse($master->id());

      if (empty($catalog) && $master->field_inactive->value) {
        // Do not create new $catalog entries for inactive course masters.
        $result = " - skipped: ($nid)";
      }
      else {
        $nid = \Drupal::service('hfc_course_master_tasks')->pushToCatalog($master, $catalog);
        if ($nid > 0) {
          $result = " - success: ($nid)";
        }
        else {
          \Drupal::messenger()->addError(t('@title - Copy Failed', ['@title' => $master->label()]));
          $result = " - failed";
          $context['sandbox']['fail_count']++;
        }
      }
      $context['sandbox']['progress']++;
      $context['sandbox']['current_id'] = $master->id();
      $context['message'] = Html::escape($master->label());
      $context['results'][] = $master->id() . ' : ' . Html::escape($master->label()) . $result;
      usleep(!empty($loop_delay) ? $loop_delay : 50000);
    }

    // Limit batch to five failures.
    if ($context['sandbox']['fail_count'] > 4) {
      $context['sandbox']['progress'] = $context['sandbox']['max'];
    }

    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function catalogBulkUpdateFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One Course Master processed.', '@count Course Masters processed.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }

}
